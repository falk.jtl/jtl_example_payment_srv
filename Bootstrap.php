<?php
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 * @package       jtl-shop
 */

namespace Plugin\jtl_example_payment_srv;

use JTL\Events\Dispatcher;
use JTL\IO\IO;
use JTL\Plugin\Bootstrapper;
use JTL\Smarty\JTLSmarty;
use Plugin\jtl_example_payment_srv\src\Backend\CustomLink;
use Plugin\jtl_example_payment_srv\src\Backend\Install;
use Plugin\jtl_example_payment_srv\src\Frontend\AjaxIO;

/**
 * Class Bootstrap
 * @package Plugin\jtl_example_payment_srv
 */
class Bootstrap extends Bootstrapper
{
    /**
     * @inheritDoc
     */
    public function installed()
    {
        parent::installed();

        $installer = new Install($this->getPlugin(), $this->getDB());
        $installer->install();
    }

    /**
     * @inheritDoc
     */
    public function uninstalled(bool $deleteData = true)
    {
        parent::uninstalled();

        $installer = new Install($this->getPlugin(), $this->getDB());
        $installer->uninstall($deleteData);
    }

    /**
     * @inheritDoc
     */
    public function renderAdminMenuTab(string $tabName, int $menuID, JTLSmarty $smarty): string
    {
        $plugin = $this->getPlugin();
        $menu   = $plugin->getAdminMenu()->getItems()->first(static function ($item) use ($menuID) {
            return $item->kPluginAdminMenu === $menuID;
        });

        switch ($tabName) {
            case 'Merchants':
                return CustomLink::handleRequest($plugin, $menu, $_POST['merchants'] ?? [])->getOutput();
            case 'Customers':
                return CustomLink::handleRequest($plugin, $menu, $_POST['customers'] ?? [])->getOutput();
            case 'Transactions':
                return CustomLink::handleRequest($plugin, $menu, $_POST['transactions'] ?? [])->getOutput();
            case 'Payments':
                return CustomLink::handleRequest($plugin, $menu, $_POST['payments'] ?? [])->getOutput();
        }

        return parent::renderAdminMenuTab($tabName, $menuID, $smarty);
    }

    /**
     * @inheritDoc
     */
    public function boot(Dispatcher $dispatcher): void
    {
        parent::boot($dispatcher);

        $dispatcher->listen('shop.hook.' . \HOOK_IO_HANDLE_REQUEST, static function (array $args) {
            /** @var IO $io */
            $io = $args['io'];

            /** @var string $request */
            $request = $args['request'];
            $ro      = \json_decode($request, true);
            if (isset($ro['name']) && $ro['name'] === AjaxIO::ENTRYKEY) {
                $io->register(AjaxIO::ENTRYKEY, [AjaxIO::class, AjaxIO::ENTRYPOINT]);
            }
        });
    }
}
