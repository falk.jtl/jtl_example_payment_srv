# JTL Beispiel Zahlungs-Plugin (Server)

Achtung! - Dieses Plugin dient nur als Beispiel für Entwickler! Dies ist kein echtes Zahlungs-Plugin!
Im Zusammenspiel mit dem [JTL Beispiel Zahlungs-Plugin](https://gitlab.com/falk.jtl/jtl_example_payment)
dient dies als Server Komponente zur Demonstration von Zahlungen.

## Voraussetzungen

* JTL-Shop5 ab 5.0.0-rc.2

## Related Links

* [JTL Beispiel Zahlungs-Plugin](https://gitlab.com/falk.jtl/jtl_example_payment)
* [JTL Beispiel Zahlungs-Plugin (Server)](https://gitlab.com/falk.jtl/jtl_example_payment_srv)