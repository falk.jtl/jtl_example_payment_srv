<form method="post" action="plugin.php?kPlugin={$kPlugin}" enctype="multipart/form-data" name="mapping">
    {$jtl_token}
    <input type="hidden" name="kPlugin" value="{$kPlugin}" />
    <input type="hidden" name="kPluginAdminMenu" value="{$kPluginAdminMenu}" />

    <div class="description mb-4">{__('Manage customers for the example payment plugin here')}</div>
    <div class="mb-4">
        <button class="btn btn-primary" name="customers[action]" value="create"><span class="fal fa-plus mr-2"></span>{__('Create new customer')}</button>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-align-top">
                    <thead>
                    <tr>
                        <th>{__('ID')}</th>
                        <th>{__('Name')}</th>
                        <th class="text-right">{__('Credit')}</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        {foreach name=customer from=$customers item=customer}
                        <tr>
                            <td>{$customer->id}</td>
                            <td>{$customer->customer_name}</td>
                            <td class="text-right">{$customer->credit|number_format:2:',':'.'}&nbsp;&euro;</td>
                            <td>
                                <div class="btn-group">
                                    <button name="customers[quickaction][delete]" value="{$customer->id}" class="btn btn-link px-2" title="{__('Delete customer')}" data-toggle="tooltip">
                                        <span class="icon-hover">
                                            <span class="fal fa-trash-alt"></span>
                                            <span class="fas fa-trash-alt"></span>
                                        </span>
                                    </button>
                                    <button name="customers[quickaction][edit]" value="{$customer->id}" class="btn btn-link px-2" title="{__('Edit customer')}" data-toggle="tooltip">
                                        <span class="icon-hover">
                                            <span class="fal fa-edit"></span>
                                            <span class="fas fa-edit"></span>
                                        </span>
                                    </button>
                                    <button name="customers[quickaction][refresh]" value="{$customer->id}" class="btn btn-link px-2" title="{__('Refresh customer')}" data-toggle="tooltip">
                                        <span class="icon-hover">
                                            <span class="fal fa-refresh"></span>
                                            <span class="fas fa-refresh"></span>
                                        </span>
                                    </button>
                                </div>
                            </td>
                        </tr>
                        {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</form>