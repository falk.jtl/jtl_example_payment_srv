<form method="post" action="plugin.php?kPlugin={$kPlugin}" enctype="multipart/form-data" name="mapping">
    {$jtl_token}
    <input type="hidden" name="kPlugin" value="{$kPlugin}" />
    <input type="hidden" name="kPluginAdminMenu" value="{$kPluginAdminMenu}" />
    <input type="hidden" name="customers[customerID]" value="{$customer->id}" />

    <div class="description mb-4 alert alert-danger">
        {__('Are you really sure you will delete this customer')}
    </div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <span class="label label-default col col-sm-4 col-form-label">{__('Name')}</span>
                <span>{$customer->customer_name}</span>
            </div>
        </div>
    </div>
    <div class="save-wrapper">
        <div class="row">
            <div class="ml-auto col-sm-6 col-lg-auto">
                <button name="customers[action]" value="overview" class="btn btn-outline-primary btn-block"><i class="fa fa-times"></i> {__('No')}</button>
            </div>
            <div class="col-sm-6 col-lg-auto">
                <button name="customers[action]" value="delete" class="btn btn-primary btn-block"><i class="fa fa-check"></i> {__('Yes')}</button>
            </div>
        </div>
    </div>
</form>