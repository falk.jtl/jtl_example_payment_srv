<form method="post" action="plugin.php?kPlugin={$kPlugin}" enctype="multipart/form-data" name="mapping">
    {$jtl_token}
    <input type="hidden" name="kPlugin" value="{$kPlugin}" />
    <input type="hidden" name="kPluginAdminMenu" value="{$kPluginAdminMenu}" />
    <input type="hidden" name="merchants[merchantID]" value="{$merchant->id}" />

    <div class="description mb-4 alert alert-danger">
        {__('Are you really sure you will delete this merchant')}
    </div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <span class="label label-default col col-sm-4 col-form-label">{__('Name')}</span>
                <span>{$merchant->merchant_name}</span>
            </div>
            <div class="row">
                <span class="label label-default col col-sm-4 col-form-label">{__('ID')}</span>
                <span>{$merchant->merchant_id}</span>
            </div>
        </div>
    </div>
    <div class="save-wrapper">
        <div class="row">
            <div class="ml-auto col-sm-6 col-lg-auto">
                <button name="merchants[action]" value="overview" class="btn btn-outline-primary btn-block"><i class="fa fa-times"></i> {__('No')}</button>
            </div>
            <div class="col-sm-6 col-lg-auto">
                <button name="merchants[action]" value="delete" class="btn btn-primary btn-block"><i class="fa fa-check"></i> {__('Yes')}</button>
            </div>
        </div>
    </div>
</form>