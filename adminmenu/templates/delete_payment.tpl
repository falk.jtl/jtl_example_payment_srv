<form method="post" action="plugin.php?kPlugin={$kPlugin}" enctype="multipart/form-data" name="mapping">
    {$jtl_token}
    <input type="hidden" name="kPlugin" value="{$kPlugin}" />
    <input type="hidden" name="kPluginAdminMenu" value="{$kPluginAdminMenu}" />
    <input type="hidden" name="payments[paymentID]" value="{$payments->id}" />

    <div class="description mb-4 alert alert-danger">
        {__('Are you really sure you will delete this payment')}
    </div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <span class="label label-default col col-sm-4 col-form-label">{__('ID')}</span>
                <span>{$payments->transaction_key}</span>
            </div>
            <div class="row">
                <span class="label label-default col col-sm-4 col-form-label">{__('Name')}</span>
                <span>{$payments->transaction_name}</span>
            </div>
            <div class="row">
                <span class="label label-default col col-sm-4 col-form-label">{__('Merchant')} / {__('Customer')}</span>
                <span>{if empty($payments->merchant_name)}&mdash;{else}{$payments->merchant_name}{/if} / {if empty($payments->customer_name)}&mdash;{else}{$payments->customer_name}{/if}</span>
            </div>
            <div class="row">
                <span class="label label-default col col-sm-4 col-form-label">{__('Date')}</span>
                <span>{$payments->transaction_date->format('d.m.Y H:i:s')}</span>
            </div>
        </div>
    </div>
    <div class="save-wrapper">
        <div class="row">
            <div class="ml-auto col-sm-6 col-lg-auto">
                <button name="payments[action]" value="overview" class="btn btn-outline-primary btn-block"><i class="fa fa-times"></i> {__('No')}</button>
            </div>
            <div class="col-sm-6 col-lg-auto">
                <button name="payments[action]" value="delete" class="btn btn-primary btn-block"><i class="fa fa-check"></i> {__('Yes')}</button>
            </div>
        </div>
    </div>
</form>