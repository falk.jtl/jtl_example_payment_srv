<form method="post" action="plugin.php?kPlugin={$kPlugin}" enctype="multipart/form-data" name="mapping">
    {$jtl_token}
    <input type="hidden" name="kPlugin" value="{$kPlugin}" />
    <input type="hidden" name="kPluginAdminMenu" value="{$kPluginAdminMenu}" />
    <input type="hidden" name="customers[customer][id]" value="{$customer->id}" />

    <div class="card">
        <div class="card-header">
            <div class="subheading1">{if $customer->id > 0}{__('Edit customer')}{else}{__('Create new customer')}{/if}</div>
        </div>
        <div class="card-body">
            <div class="form-group form-row align-items-center">
                <label class="col col-sm-3 col-form-label" for="customer_name">{__('Name')}</label>
                <div class="col-sm">
                    <input class="form-control" type="text" id="customer_name" name="customers[customer][name]" value="{$customer->customer_name}" required aria-required="true" >
                </div>
            </div>
            <div class="form-group form-row align-items-center">
                <label class="col col-sm-3 col-form-label" for="customer_secret">{__('Secret')}</label>
                <div class="col-sm">
                    <input class="form-control" type="password" id="customer_secret" name="customers[customer][secret]" value="" >
                </div>
            </div>
            <div class="form-group form-row align-items-center">
                <label class="col col-sm-3 col-form-label" for="customer_credit">{__('Credit')}</label>
                <div class="col-sm">
                    <input class="form-control" type="text" id="customer_credit" name="customers[customer][credit]" value="{$customer->credit|number_format:2:',':'.'}" >
                </div>
            </div>
        </div>
    </div>
    <div class="save-wrapper">
        <div class="row">
            <div class="ml-auto col-sm-6 col-lg-auto">
                <button name="customers[action]" value="overview" class="btn btn-outline-primary btn-block"><i class="fa fa-exclamation"></i> {__('Cancel')}</button>
            </div>
            <div class="col-sm-6 col-lg-auto">
                <button name="customers[action]" value="save" class="btn btn-primary btn-block"><i class="fa fa-save"></i> {__('Save')}</button>
            </div>
        </div>
    </div>
</form>