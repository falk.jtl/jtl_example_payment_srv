<form method="post" action="plugin.php?kPlugin={$kPlugin}" enctype="multipart/form-data" name="mapping">
    {$jtl_token}
    <input type="hidden" name="kPlugin" value="{$kPlugin}" />
    <input type="hidden" name="kPluginAdminMenu" value="{$kPluginAdminMenu}" />
    <input type="hidden" name="merchants[merchant][id]" value="{$merchant->id}" />

    <div class="card">
        <div class="card-header">
            <div class="subheading1">{if $merchant->id > 0}{__('Edit merchant')}{else}{__('Create new merchant')}{/if}</div>
        </div>
        <div class="card-body">
            <div class="form-group form-row align-items-center">
                <label class="col col-sm-3 col-form-label" for="merchant_name">{__('Name')}</label>
                <div class="col-sm">
                    <input class="form-control" type="text" id="merchant_name" name="merchants[merchant][name]" value="{$merchant->merchant_name}" required aria-required="true" >
                </div>
            </div>
            <div class="form-group form-row align-items-center">
                <label class="col col-sm-3 col-form-label" for="merchant_key">{__('ID')}</label>
                <div class="col-sm">
                    <input class="form-control" type="text" id="merchant_key" name="merchants[merchant][key]" value="{$merchant->merchant_id}" >
                </div>
            </div>
            <div class="form-group form-row align-items-center">
                <label class="col col-sm-3 col-form-label" for="merchant_secret">{__('Secret')}</label>
                <div class="col-sm">
                    <input class="form-control" type="text" id="merchant_secret" name="merchants[merchant][secret]" value="{$merchant->secret}" >
                </div>
            </div>
            <div class="form-group form-row align-items-center">
                <label class="col col-sm-3 col-form-label" for="merchant_credit">{__('Credit')}</label>
                <div class="col-sm">
                    <input class="form-control" type="text" id="merchant_credit" name="merchants[merchant][credit]" value="{$merchant->credit|number_format:2:',':'.'}" >
                </div>
            </div>
        </div>
    </div>
    <div class="save-wrapper">
        <div class="row">
            <div class="ml-auto col-sm-6 col-lg-auto">
                <button name="merchants[action]" value="overview" class="btn btn-outline-primary btn-block"><i class="fa fa-exclamation"></i> {__('Cancel')}</button>
            </div>
            <div class="col-sm-6 col-lg-auto">
                <button name="merchants[action]" value="save" class="btn btn-primary btn-block"><i class="fa fa-save"></i> {__('Save')}</button>
            </div>
        </div>
    </div>
</form>