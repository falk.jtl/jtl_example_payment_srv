<form method="post" action="plugin.php?kPlugin={$kPlugin}" enctype="multipart/form-data" name="mapping">
    {$jtl_token}
    <input type="hidden" name="kPlugin" value="{$kPlugin}" />
    <input type="hidden" name="kPluginAdminMenu" value="{$kPluginAdminMenu}" />
    <input type="hidden" name="transactions[transaction][id]" value="{$transactions->id}" />

    <div class="card">
        <div class="card-header">
            <div class="subheading1">{if $transactions->id > 0}{__('Edit transaction')}{else}{__('Create new transaction')}{/if}</div>
        </div>
        <div class="card-body">
            <div class="form-group form-row align-items-center">
                <label class="col col-sm-3 col-form-label" for="transaction_name">{__('Name')}</label>
                <div class="col-sm">
                    <input class="form-control" type="text" id="transaction_name" name="transactions[transaction][name]" value="{$transactions->transaction_name}" required aria-required="true" >
                </div>
            </div>
            <div class="form-group form-row align-items-center">
                <label class="col col-sm-3 col-form-label" for="merchant_id">{__('Merchant')}</label>
                <div class="col-sm">
                    <select class="form-control" id="merchant_id" name="transactions[transaction][merchant_id]">
                        <option value="">&ndash;</option>
                        {foreach name=merchants from=$merchantsList item=merchant}
                            <option value="{$merchant->id}"{if $merchant->selected} selected{/if}>{$merchant->merchant_name}</option>
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="form-group form-row align-items-center">
                <label class="col col-sm-3 col-form-label" for="customer_id">{__('Customer')}</label>
                <div class="col-sm">
                    <select class="form-control" id="customer_id" name="transactions[transaction][customer_id]">
                        <option value="">&ndash;</option>
                        {foreach name=customers from=$customersList item=customer}
                            <option value="{$customer->id}"{if $customer->selected} selected{/if}>{$customer->customer_name}</option>
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="form-group form-row align-items-center">
                <label class="col col-sm-3 col-form-label" for="transaction_date">{__('Date')}</label>
                <div class="col-sm">
                    <input class="form-control" type="datetime-local" id="transaction_date" name="transactions[transaction][date]" value="{$transactions->transaction_date->format('Y-m-d\TH:i:s')}" required aria-required="true">
                </div>
            </div>
            <div class="form-group form-row align-items-center">
                <label class="col col-sm-3 col-form-label" for="transaction_value">{__('Value')}</label>
                <div class="col-sm">
                    <input class="form-control" type="text" id="transaction_value" name="transactions[transaction][value]" value="{$transactions->transaction_value|number_format:2:',':'.'}" required aria-required="true" >
                </div>
            </div>
        </div>
    </div>
    <div class="save-wrapper">
        <div class="row">
            <div class="ml-auto col-sm-6 col-lg-auto">
                <button name="transactions[action]" value="overview" class="btn btn-outline-primary btn-block"><i class="fa fa-exclamation"></i> {__('Cancel')}</button>
            </div>
            <div class="col-sm-6 col-lg-auto">
                <button name="transactions[action]" value="save" class="btn btn-primary btn-block"><i class="fa fa-save"></i> {__('Save')}</button>
            </div>
        </div>
    </div>
</form>