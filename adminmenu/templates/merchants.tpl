<form method="post" action="plugin.php?kPlugin={$kPlugin}" enctype="multipart/form-data" name="mapping">
    {$jtl_token}
    <input type="hidden" name="kPlugin" value="{$kPlugin}" />
    <input type="hidden" name="kPluginAdminMenu" value="{$kPluginAdminMenu}" />

    <div class="description mb-4">{__('Manage merchants for the example payment plugin here')}</div>
    <div class="mb-4">
        <button class="btn btn-primary" name="merchants[action]" value="create"><span class="fal fa-plus mr-2"></span>{__('Create new merchant')}</button>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-align-top">
                    <thead>
                        <tr>
                            <th>{__('ID')}</th>
                            <th>{__('Name')}</th>
                            <th>{__('Secret')}</th>
                            <th class="text-right">{__('Credit')}</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach name=merchant from=$merchants item=merchant}
                        <tr>
                            <td>{$merchant->merchant_id}</td>
                            <td>{$merchant->merchant_name}</td>
                            <td>{$merchant->secret}</td>
                            <td class="text-right">{$merchant->credit|number_format:2:',':'.'}&nbsp;&euro;</td>
                            <td>
                                <div class="btn-group">
                                    <button name="merchants[quickaction][delete]" value="{$merchant->id}" class="btn btn-link px-2" title="{__('Delete merchant')}" data-toggle="tooltip">
                                        <span class="icon-hover">
                                            <span class="fal fa-trash-alt"></span>
                                            <span class="fas fa-trash-alt"></span>
                                        </span>
                                    </button>
                                    <button name="merchants[quickaction][edit]" value="{$merchant->id}" class="btn btn-link px-2" title="{__('Edit merchant')}" data-toggle="tooltip">
                                        <span class="icon-hover">
                                            <span class="fal fa-edit"></span>
                                            <span class="fas fa-edit"></span>
                                        </span>
                                    </button>
                                    <button name="merchants[quickaction][refresh]" value="{$merchant->id}" class="btn btn-link px-2" title="{__('Refresh merchant')}" data-toggle="tooltip">
                                        <span class="icon-hover">
                                            <span class="fal fa-refresh"></span>
                                            <span class="fas fa-refresh"></span>
                                        </span>
                                    </button>
                                </div>
                            </td>
                        </tr>
                        {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</form>