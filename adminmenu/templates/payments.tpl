{assign var="selectedMerchant" value=""}
{assign var="selectedCustomer" value=""}
<form method="post" action="plugin.php?kPlugin={$kPlugin}" enctype="multipart/form-data" name="mapping">
    {$jtl_token}
    <input type="hidden" name="kPlugin" value="{$kPlugin}" />
    <input type="hidden" name="kPluginAdminMenu" value="{$kPluginAdminMenu}" />

    <div class="description mb-4">{__('Manage payments for the example payment plugin here')}</div>
    <div class="mb-4">
        <button class="btn btn-primary" name="payments[action]" value="create"><span class="fal fa-plus mr-2"></span>{__('Create new payment')}</button>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="toolbar">
                <div class="form-row align-items-end">
                    <div class="col-md-6 col-lg-3 col-xl-2">
                        <div class="form-group">
                            <label for="filterMerchant">{__('Merchant')}</label>
                            <select class="custom-select" id="filterMerchant" name="payments[filter][merchant]">
                                <option value="">&ndash;</option>
                                {foreach name=merchants from=$merchantsList item=merchant}
                                    <option value="{$merchant->id}"{if $merchant->selected}{assign var="selectedMerchant" value=$merchant->selected} selected{/if}>{$merchant->merchant_name}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3 col-xl-2">
                        <div class="form-group">
                            <label for="filterCustomer">{__('Customer')}</label>
                            <select class="custom-select" id="filterCustomer" name="payments[filter][customer]">
                                <option value="">&ndash;</option>
                                {foreach name=customers from=$customersList item=customer}
                                    <option value="{$customer->id}"{if $customer->selected}{assign var="selectedCustomer" value=$customer->selected} selected{/if}>{$customer->customer_name}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="col-md-auto">
                        <div class="form-group mt-2">
                            <button type="submit" class="btn btn-outline-primary btn-block" name="payments[action]" value="resetFilter"><i class="fal fa-eraser"></i></button>
                        </div>
                    </div>
                    <div class="col-md-auto">
                        <div class="form-group mt-2">
                            <button type="submit" class="btn btn-primary btn-block" name="payments[action]" value="overview"><i class="fal fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-align-top">
                    <thead>
                    <tr>
                        <th>{__('ID')}</th>
                        <th>{__('Name')}</th>
                        <th>{__('Merchant')}</th>
                        <th>{__('Customer')}</th>
                        <th>{__('Date')}</th>
                        <th class="text-right">{__('Value')}</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach name=payment from=$pagination->getPageItems() item=payment}
                        <tr>
                            <td>{$payment->payment_key|substr:0:4}&hellip;{$payment->payment_key|substr:-6}</td>
                            <td>{$payment->transaction_name}</td>
                            <td>{$payment->merchant_name}</td>
                            <td>{$payment->customer_name}</td>
                            <td>{$payment->transaction_date->format('d.m.Y H:i:s')}</td>
                            <td class="text-right">{$payment->transaction_value|number_format:2:',':'.'}&nbsp;&euro;</td>
                            <td>
                                <div class="btn-group">
                                    <button name="payments[quickaction][delete]" value="{$payment->id}" class="btn btn-link px-2" title="{__('Delete payment')}" data-toggle="tooltip">
                                        <span class="icon-hover">
                                            <span class="fal fa-trash-alt"></span>
                                            <span class="fas fa-trash-alt"></span>
                                        </span>
                                    </button>
                                    <button name="payments[quickaction][edit]" value="{$payment->id}" class="btn btn-link px-2" title="{__('Edit payment')}" data-toggle="tooltip">
                                        <span class="icon-hover">
                                            <span class="fal fa-edit"></span>
                                            <span class="fas fa-edit"></span>
                                        </span>
                                    </button>
                                    <button name="payments[quickaction][accept]" value="{$payment->id}" class="btn btn-link px-2" title="{__('Accept payment')}" data-toggle="tooltip">
                                        <span class="icon-hover">
                                            <span class="fal fa-check"></span>
                                            <span class="fas fa-check"></span>
                                        </span>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</form>
{include file='tpl_inc/pagination.tpl'
    pagination=$pagination
    cParam_arr=[
        'kPlugin' => {$kPlugin},
        'kPluginAdminMenu' => {$kPluginAdminMenu},
        'transactions[filter][merchant]' => $selectedMerchant,
        'transactions[filter][customer]' => $selectedCustomer
    ]
}
