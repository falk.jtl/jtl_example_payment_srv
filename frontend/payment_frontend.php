<?php
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 * @package       jtl-shop
 */

use JTL\Plugin\PluginInterface;
use Plugin\jtl_example_payment_srv\src\Frontend\FrontendProvider;

/** @global PluginInterface $plugin */
(new FrontendProvider($plugin))->run('paymentFrontend');
