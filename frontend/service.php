<?php
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 * @package       jtl-shop
 */

use Plugin\jtl_example_payment_srv\src\Frontend\ServiceProvider;

(new ServiceProvider())->run();
