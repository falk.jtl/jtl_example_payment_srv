{form id="form_customer_login" class="evo-validate label-slide payment_extra" method="post" action=""}
    <div id="order-additional-payment" class="bottom15 form-group">
        <fieldset>
            <input type="hidden" name="data[post]" value="1">
            <legend>{$pluginLocale->getTranslation('prepayed_payment_head_login')}</legend>
            <p>{$pluginLocale->getTranslation('prepayed_payment_desc_login')}</p>
            {row}
                {col cols=12 md=6}
                    {formgroup label-for="prepaid_card_name"
                        label={lang section="checkout" key="accountHolder"}
                    }
                        {input id="prepaid_card_name" type="text" name="data[customer]" value=$credentials_loginName placeholder={lang section="checkout" key="accountHolder"} required=true autocomplete="username email"}
                    {/formgroup}
                {/col}
                {col cols=12 md=6}
                    {formgroup label-for="prepaid_card_password"
                        label={lang section="global" key="password"}
                    }
                        {input id="prepaid_card_password" type="password" name="data[secret]" value=$credentials_secret placeholder={lang section="global" key="password"} required=true autocomplete="off"}
                    {/formgroup}
                {/col}
            {/row}
        </fieldset>
    </div>
    <div class="text-right">
        <a name ='data[task]' class="submit btn btn-lg submit_once btn-secondary" href="{$cancelURL}" >{lang key='back' section='global'}</a>
        <button name ='data[task]' type="submit" value="login" class="submit btn btn-lg submit_once btn-primary" >{lang key='login' section='global'}</button>
    </div>
{/form}
