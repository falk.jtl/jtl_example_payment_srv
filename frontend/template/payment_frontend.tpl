{if $task === 'form'}
    {include file="layout/header.tpl"}
    {include file="{$plugin->getPaths()->getFrontendPath()}/template/customer_login.tpl"}
    {include file="layout/footer.tpl"}
{elseif $task === 'sendBack'}
    {include file="layout/modal_header.tpl"}
    <form id="form_frontend_send" class="evo-validate label-slide payment_extra" method="post" action={$returnURL}>
        <input type="hidden" name="{$context}[response]" value="{$response}">
        <button type="submit" class="submit btn btn-lg submit_once btn-primary" >{lang key='redirect' section='global'}</button>
    </form>
    <script type="application/javascript">
        document.getElementById('form_frontend_send').submit();
    </script>
    {include file="layout/modal_footer.tpl"}
{/if}
