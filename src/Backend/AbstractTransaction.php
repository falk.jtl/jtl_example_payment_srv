<?php
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 * @package       jtl-shop
 */

namespace Plugin\jtl_example_payment_srv\src\Backend;

use DateTime;
use Illuminate\Support\Collection;
use JTL\DB\ReturnType;
use JTL\Plugin\PluginInterface;
use JTL\Shop;

/**
 * Class AbstractTransaction
 * @package Plugin\jtl_example_payment_srv\src\Backend
 */
abstract class AbstractTransaction
{
    /** @var PluginInterface */
    private $plugin;

    /** @var int */
    private $customerID = 0;

    /** @var int */
    private $merchantID = 0;

    /** @var string */
    protected $context = '';

    /** @var string */
    protected $merchantKey = '';

    /** @var string */
    protected $customerKey = '';

    /** @var string */
    protected $limitStr = '';

    /**
     * Transactions constructor.
     * @param PluginInterface $plugin
     * @param array           $filter
     */
    public function __construct(PluginInterface $plugin, array $filter = [])
    {
        $this->plugin  = $plugin;
        $this->context = $this->getContext();

        $this->merchantKey = 'jtl_example_payment_srv.' . $this->context . '.merchant';
        $this->customerKey = 'jtl_example_payment_srv.' . $this->context . '.customer';

        if (isset($filter['merchant'])) {
            $this->setMerchantID((int)$filter['merchant']);
        } elseif (isset($_SESSION[$this->merchantKey])) {
            $this->setMerchantID((int)$_SESSION[$this->merchantKey]);
        }

        if (isset($filter['customer'])) {
            $this->setCustomerID((int)$filter['customer']);
        } elseif (isset($_SESSION[$this->customerKey])) {
            $this->setCustomerID((int)$_SESSION[$this->customerKey]);
        }
    }

    /**
     * @return string
     */
    abstract protected function getContext(): string;

    /**
     * @param int $customerID
     */
    protected function setCustomerID(int $customerID): void
    {
        $this->customerID = $customerID;

        $_SESSION[$this->customerKey] = $this->customerID;
    }

    /**
     * @param int $merchantID
     */
    protected function setMerchantID(int $merchantID): void
    {
        $this->merchantID = $merchantID;

        $_SESSION[$this->merchantKey] = $this->merchantID;
    }

    /**
     * @return PluginInterface
     */
    protected function getPlugin(): PluginInterface
    {
        return $this->plugin;
    }

    /**
     * @param int $start
     * @param int $limit
     * @return static
     */
    public function setLimit(int $start = 0, int $limit = 0)
    {
        if ($start === 0 && $limit === 0) {
            $this->limitStr = '';

            return $this;
        }
        if ($start === 0) {
            $this->limitStr = (string)$limit;

            return $this;
        }
        $this->limitStr = $start . ', ' . $limit;

        return $this;
    }

    /**
     * @param array $filter
     * @param array $params
     * @return mixed
     */
    abstract protected function queryTransactions(array $filter, array $params);

    /**
     * @param int $merchantID
     * @param int $customerID
     * @return Collection|null
     */
    public function getTransactions(int $merchantID = 0, int $customerID = 0): ?Collection
    {
        $filter = [];
        $params = [];

        if ($merchantID !== 0) {
            $filter['merchantID'] = 't.merchant_id = :merchantID';
            $params['merchantID'] = $merchantID;
        }
        if ($customerID !== 0) {
            $filter['customerID'] = 't.customer_id = :customerID';
            $params['customerID'] = $customerID;
        }

        /** @var Collection $transactions */
        $transactions = $this->queryTransactions($filter, $params);

        if (\is_a($transactions, Collection::class)) {
            $transactions->map(static function ($item) {
                try {
                    $item->transaction_date = new DateTime($item->transaction_date);
                } catch (\Exception $e) {
                    $item->transaction_date = null;
                }

                return $item;
            });

            return $transactions;
        }

        return null;
    }

    /**
     * @return int
     */
    public function getTransactionsCount(): int
    {
        $row = Shop::Container()->getDB()->query('SELECT FOUND_ROWS() AS rowCount', ReturnType::SINGLE_OBJECT);

        return \is_object($row) ? $row->rowCount : 0;
    }

    /**
     * @param int $id
     * @return mixed
     */
    abstract protected function queryTransaction(int $id);

    /**
     * @param int $id
     * @return object|null
     */
    protected function getTransaction(int $id): ?object
    {
        $transaction = $this->queryTransaction($id);

        if (\is_object($transaction)) {
            try {
                $transaction->transaction_date = new DateTime($transaction->transaction_date);
            } catch (\Exception $e) {
                $transaction->transaction_date = null;
            }

            return $transaction;
        }

        return null;
    }

    /**
     * @return object
     */
    protected static function emptyTransaction(): object
    {
        try {
            return (object)[
                'transaction_name'  => '',
                'merchant_id'       => null,
                'customer_id'       => null,
                'transaction_date'  => new DateTime(),
                'transaction_value' => 0,
            ];
        } catch (\Exception $e) {
            return (object)[
                'transaction_name'  => '',
                'merchant_id'       => null,
                'customer_id'       => null,
                'transaction_date'  => '1999-01-01 00:00:00',
                'transaction_value' => 0,
            ];
        }
    }

    /**
     * @param array $data
     * @return array
     */
    protected static function mapData(array $data): array
    {
        $data['transaction_name']  = $data['name'];
        $data['transaction_date']  = $data['date'];
        $data['transaction_value'] = str_replace(['.', ','], ['', '.'], $data['value']);

        return $data;
    }

    /**
     * @return void
     */
    public function resetFilter(): void
    {
        $this->setMerchantID(0);
        $this->setCustomerID(0);
    }

    /**
     * @param int $id
     * @return void
     */
    public function willDelete(int $id): void
    {
        Shop::Smarty()->assign($this->context, $this->getTransaction($id));
    }

    /**
     * @param int $id
     * @return void
     */
    public function edit(int $id): void
    {
        $transaction = $id === 0 ? static::emptyTransaction() : $this->getTransaction($id);
        Shop::Smarty()
            ->assign($this->context, $transaction)
            ->assign('merchantsList', (new Merchants($this->getPlugin()))->list($transaction->merchant_id ?? 0))
            ->assign('customersList', (new Customers($this->getPlugin()))->list($transaction->customer_id ?? 0));
    }

    /**
     * @param array $data
     * @return void
     */
    abstract public function save(array $data): void;

    /**
     * @param int $id
     * @return void
     */
    abstract public function delete(int $id): void;

    /**
     * @return Collection
     */
    public function overview(): Collection
    {
        Shop::Smarty()
            ->assign('merchantsList', (new Merchants($this->plugin))->list($this->merchantID))
            ->assign('customersList', (new Customers($this->plugin))->list($this->customerID));

        return $this->getTransactions($this->merchantID, $this->customerID) ?? new Collection();
    }
}
