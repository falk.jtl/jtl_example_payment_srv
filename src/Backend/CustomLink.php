<?php
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 * @package       jtl-shop
 */

namespace Plugin\jtl_example_payment_srv\src\Backend;

use Illuminate\Support\Collection;
use JTL\Alert\Alert;
use JTL\Helpers\Form;
use JTL\Helpers\Request;
use JTL\Helpers\Text;
use JTL\Pagination\Pagination;
use JTL\Plugin\PluginInterface;
use JTL\Shop;

/**
 * Class CustomLink
 * @package Plugin\jtl_example_payment_srv\src\Backend
 */
class CustomLink
{
    /** @var PluginInterface */
    private $plugin;

    /** @var object */
    private $menu;

    /** @var array */
    private $requestData;

    /** @var string */
    private $task;

    /** @var string */
    private $output = '';

    /** @var Pagination */
    private $pagination;

    /**
     * CustomLink constructor.
     * @param PluginInterface $plugin
     * @param object          $menu
     * @param array           $requestData
     */
    private function __construct(PluginInterface $plugin, object $menu, array $requestData = [])
    {
        $this->plugin      = $plugin;
        $this->menu        = $menu;
        $this->requestData = Text::filterXSS($requestData);
        $this->task        = strtolower($menu->name);
    }

    /**
     * @param PluginInterface $plugin
     * @param object          $menu
     * @param array           $requestData
     * @return CustomLink
     */
    public static function handleRequest(PluginInterface $plugin, object $menu, array $requestData = []): self
    {
        $instance = new static($plugin, $menu, $requestData);
        if ($instance->controller()) {
            return $instance->render();
        }

        return $instance;
    }

    /**
     * @return bool
     * @uses taskMerchants
     * @uses taskCustomers
     * @uses taskTransactions
     * @uses taskPayments
     */
    protected function controller(): bool
    {
        Shop::Container()->getGetText()->loadPluginLocale('customlink_' . $this->task, $this->getPlugin());
        $methodName = 'task' . ucfirst($this->task);
        $limitRange = Request::postInt(
            $this->task . '_nItemsPerPage',
            Request::getInt($this->task . '_nItemsPerPage', 10)
        );
        $limitStart = Request::postInt(
            $this->task . 'nPage',
            Request::getInt($this->task . '_nPage')
        ) * $limitRange;

        if (\is_callable([$this, $methodName])) {
            return $this->$methodName($this->requestData['action'] ?? 'overview', $limitStart, $limitRange);
        }

        return true;
    }

    /**
     * @param string $subTask
     * @return string
     */
    protected function check(string $subTask): string
    {
        if (!Form::validateToken()) {
            unset($this->requestData['action'], $this->requestData['quickaction']);

            if ($subTask !== 'overview') {
                $subTask = 'overview';
                Shop::Container()->getAlertService()->addAlert(
                    Alert::TYPE_ERROR,
                    __('The token cant be recognized'),
                    'tokenFailed'
                );
            }
        }

        return $subTask;
    }

    /**
     * @param string $subTask
     * @return bool
     */
    protected function taskMerchants(string $subTask): bool
    {
        $merchants = new Merchants($this->plugin);
        $subTask   = $this->check($subTask);

        switch ($subTask) {
            case 'create':
                $merchants->edit(0);
                $this->task = 'edit_merchant';

                break;
            case 'save':
                $merchants->save($this->requestData['merchant']);
                $this->redirect();

                break;
            case 'delete':
                $merchants->delete((int)$this->requestData['merchantID']);
                $this->redirect();

                break;
            case 'overview':
            default:
                if (isset($this->requestData['quickaction']['delete'])) {
                    $merchants->willDelete((int)$this->requestData['quickaction']['delete']);
                    $this->task = 'delete_merchant';
                } elseif (isset($this->requestData['quickaction']['edit'])) {
                    $merchants->edit((int)$this->requestData['quickaction']['edit']);
                    $this->task = 'edit_merchant';
                } elseif (isset($this->requestData['quickaction']['refresh'])) {
                    $merchants->refresh((int)$this->requestData['quickaction']['refresh']);
                    $this->redirect();
                } else {
                    $merchants->overview();
                }
        }

        return true;
    }

    /**
     * @param string $subTask
     * @return bool
     */
    protected function taskCustomers(string $subTask): bool
    {
        $customers = new Customers($this->plugin);
        $subTask   = $this->check($subTask);

        switch ($subTask) {
            case 'create':
                $customers->edit(0);
                $this->task = 'edit_customer';

                break;
            case 'save':
                $customers->save($this->requestData['customer']);
                $this->redirect();

                break;
            case 'delete':
                $customers->delete((int)$this->requestData['customerID']);
                $this->redirect();

                break;
            case 'overview':
            default:
                if (isset($this->requestData['quickaction']['delete'])) {
                    $customers->willDelete((int)$this->requestData['quickaction']['delete']);
                    $this->task = 'delete_customer';
                } elseif (isset($this->requestData['quickaction']['edit'])) {
                    $customers->edit((int)$this->requestData['quickaction']['edit']);
                    $this->task = 'edit_customer';
                } elseif (isset($this->requestData['quickaction']['refresh'])) {
                    $customers->refresh((int)$this->requestData['quickaction']['refresh']);
                    $this->redirect();
                } else {
                    $customers->overview();
                }
        }

        return true;
    }

    /**
     * @param string $subTask
     * @param int    $limitStart
     * @param int    $limitRange
     * @return bool
     */
    protected function taskTransactions(string $subTask, int $limitStart = 0, int $limitRange = 0): bool
    {
        $transactions = new Transactions($this->plugin, $this->requestData['filter'] ?? []);
        $subTask      = $this->check($subTask);
        $transactions->setLimit($limitStart, $limitRange);

        switch ($subTask) {
            case 'create':
                $transactions->edit(0);
                $this->task = 'edit_transaction';

                break;
            case 'save':
                $transactions->save($this->requestData['transaction']);
                $this->redirect();

                break;
            case 'delete':
                $transactions->delete((int)$this->requestData['transactionID']);
                $this->redirect();

                break;
            case 'resetFilter':
                $transactions->resetFilter();
                $this->redirect();

                break;
            case 'overview':
            default:
                if (isset($this->requestData['quickaction']['delete'])) {
                    $transactions->willDelete((int)$this->requestData['quickaction']['delete']);
                    $this->task = 'delete_transaction';
                } elseif (isset($this->requestData['quickaction']['edit'])) {
                    $transactions->edit((int)$this->requestData['quickaction']['edit']);
                    $this->task = 'edit_transaction';
                } else {
                    $this->setPagination($transactions->overview(), $transactions->getTransactionsCount());
                }
        }

        return true;
    }

    /**
     * @param string $subTask
     * @param int    $limitStart
     * @param int    $limitRange
     * @return bool
     */
    protected function taskPayments(string $subTask, int $limitStart = 0, int $limitRange = 0): bool
    {
        $payments = new Payments($this->plugin, $this->requestData['filter'] ?? []);
        $subTask  = $this->check($subTask);
        $payments->setLimit($limitStart, $limitRange);

        switch ($subTask) {
            case 'create':
                $payments->edit(0);
                $this->task = 'edit_payment';

                break;
            case 'save':
                $payments->save($this->requestData['payment']);
                $this->redirect();

                break;
            case 'delete':
                $payments->delete((int)$this->requestData['paymentID']);
                $this->redirect();

                break;
            case 'resetFilter':
                $payments->resetFilter();
                $this->redirect();

                break;
            case 'overview':
            default:
                if (isset($this->requestData['quickaction']['delete'])) {
                    $payments->willDelete((int)$this->requestData['quickaction']['delete']);
                    $this->task = 'delete_payment';
                } elseif (isset($this->requestData['quickaction']['edit'])) {
                    $payments->edit((int)$this->requestData['quickaction']['edit']);
                    $this->task = 'edit_payment';
                } elseif (isset($this->requestData['quickaction']['accept'])) {
                    $payments->accept((int)$this->requestData['quickaction']['accept']);
                    $this->redirect();
                } else {
                    $this->setPagination($payments->overview(), $payments->getTransactionsCount());
                }
        }

        return true;
    }

    /**
     * @return string
     */
    protected function getTemplate(): string
    {
        return 'templates/' . $this->task . '.tpl';
    }

    /**
     * @return PluginInterface
     */
    public function getPlugin(): PluginInterface
    {
        return $this->plugin;
    }

    /**
     * @return object
     */
    public function getMenu(): object
    {
        return $this->menu;
    }

    /**
     * @return array
     */
    public function getRequestData(): array
    {
        return $this->requestData;
    }

    /**
     * @return string
     */
    public function getOutput(): string
    {
        return $this->output;
    }

    /**
     * @return Pagination|null
     */
    public function getPagination(): ?Pagination
    {
        return $this->pagination;
    }

    /**
     * @param Collection $items
     * @param int        $itemCount
     */
    public function setPagination(Collection $items, int $itemCount): void
    {
        $this->pagination = (new Pagination($this->task))
            ->setRange(4)
            ->setItemArray($items)
            ->setItemCount($itemCount)
            ->assemble();
    }

    /**
     * @param string $action
     * @return void
     */
    public function redirect($action = ''): void
    {
        $params = [
            'kPlugin=' . $this->getPlugin()->getID(),
            'kPluginAdminMenu=' . $this->getMenu()->kPluginAdminMenu,
        ];
        if (!empty($action)) {
            $params[] = $this->task . '[action]=' . $action;
        }

        header('Location: ' . Shop::getURL() . '/' . PFAD_ADMIN . 'plugin.php?' . implode('&', $params), true, 302);
        exit();
    }

    /**
     * @return static
     */
    public function render()
    {
        try {
            $this->output = Shop::Smarty()
                ->assign('requestData', $this->getRequestData())
                ->assign('kPlugin', $this->getPlugin()->getID())
                ->assign('kPluginAdminMenu', $this->getMenu()->kPluginAdminMenu)
                ->assign('pagination', $this->getPagination())
                ->fetch($this->getPlugin()->getPaths()->getAdminPath() . $this->getTemplate());
        } catch (\Exception $e) {
            $this->output = $e->getMessage();
        }

        return $this;
    }
}
