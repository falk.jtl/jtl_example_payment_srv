<?php
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 * @package       jtl-shop
 */

namespace Plugin\jtl_example_payment_srv\src\Backend;

use Illuminate\Support\Collection;
use JTL\Alert\Alert;
use JTL\DB\ReturnType;
use JTL\Plugin\PluginInterface;
use JTL\Shop;
use Plugin\jtl_example_payment_srv\src\Customer;
use Plugin\jtl_example_payment_srv\src\TransactionException;

/**
 * Class Customers
 * @package Plugin\jtl_example_payment_srv\src\Backend
 */
class Customers
{
    /** @var PluginInterface */
    private $plugin;

    /**
     * Customers constructor.
     * @param PluginInterface $plugin
     */
    public function __construct(PluginInterface $plugin)
    {
        $this->plugin = $plugin;
    }

    /**
     * @return Collection|null
     */
    protected function getCustomers(): ?Collection
    {
        $customers = Shop::Container()->getDB()->query(
            'SELECT id, customer_name, credit
                FROM xplugin_jtl_example_payment_srv_customers
                ORDER BY customer_name',
            ReturnType::COLLECTION
        );

        return ($customers instanceof Collection) ? $customers : null;
    }

    /**
     * @param int $id
     * @return object|null
     */
    protected function getCustomer(int $id): ?object
    {
        $customer = Shop::Container()->getDB()->queryPrepared(
            'SELECT id, customer_name, credit
                FROM xplugin_jtl_example_payment_srv_customers
                WHERE id = :id',
            [
                'id' => $id,
            ],
            ReturnType::SINGLE_OBJECT
        );

        return \is_object($customer) ? $customer : null;
    }

    /**
     * @return object
     */
    protected static function emptyCustomer(): object
    {
        $cs = Shop::Container()->getCryptoService();

        try {
            return (object)[
                'customer_name' => '',
                'secret'        => $cs->randomString(32),
                'credit'        => 0,
            ];
        } catch (\Exception $e) {
            return (object)[
                'customer_name' => '',
                'secret'        => '',
                'credit'        => 0,
            ];
        }
    }

    /**
     * @param array $data
     * @return array
     */
    protected static function mapData(array $data): array
    {
        $data['customer_name'] = $data['name'];
        $data['credit']        = str_replace(['.', ','], ['', '.'], $data['credit']);

        return $data;
    }

    /**
     * @param int $selected
     * @return Collection
     */
    public function list(int $selected): Collection
    {
        return ($this->getCustomers() ?? new Collection())->map(static function ($item) use ($selected) {
            $item->selected = (int)$item->id === $selected;

            return $item;
        });
    }

    /**
     * @param int $id
     * @return void
     */
    public function willDelete(int $id): void
    {
        Shop::Smarty()->assign('customer', $this->getCustomer($id));
    }

    /**
     * @param int $id
     * @return void
     */
    public function edit(int $id): void
    {
        Shop::Smarty()->assign('customer', $id === 0 ? static::emptyCustomer() : $this->getCustomer($id));
    }

    /**
     * @param array $data
     * @return void
     */
    public function save(array $data): void
    {
        try {
            Customer::instance()->assign(static::mapData($data))->save();
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_SUCCESS,
                __('Customer successfully saved'),
                'customerSave',
                ['saveInSession' => true]
            );
        } catch (TransactionException $e) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                __($e->getMessage()),
                'customerSave',
                ['saveInSession' => true]
            );
        }
    }

    /**
     * @param int $id
     * @return void
     */
    public function delete(int $id): void
    {
        try {
            Customer::instance()->assign(['id' => $id])->delete();
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_SUCCESS,
                __('Customer successfully deleted'),
                'customerDeleted',
                ['saveInSession' => true]
            );
        } catch (TransactionException $e) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                __($e->getMessage()),
                'customerDeleted',
                ['saveInSession' => true]
            );
        }
    }

    /**
     * @param int $id
     * @return void
     */
    public function refresh(int $id): void
    {
        try {
            Customer::instance()->assign(['id' => $id])->refresh();
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_SUCCESS,
                __('Customer successfully refreshed'),
                'customerRefreshed',
                ['saveInSession' => true]
            );
        } catch (TransactionException $e) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                __($e->getMessage()),
                'customerDeleted',
                ['saveInSession' => true]
            );
        }
    }

    /**
     * @return void
     */
    public function overview(): void
    {
        Shop::Smarty()->assign('customers', $this->getCustomers());
    }
}
