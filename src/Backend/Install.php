<?php
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 * @package       jtl-shop
 */

namespace Plugin\jtl_example_payment_srv\src\Backend;

use JTL\Alert\Alert;
use JTL\DB\DbInterface;
use JTL\DB\ReturnType;
use JTL\Plugin\PluginInterface;
use JTL\Shop;
use Plugin\jtl_example_payment_srv\src\Customer;
use Plugin\jtl_example_payment_srv\src\Merchant;
use Plugin\jtl_example_payment_srv\src\TransactionException;

/**
 * Class Install
 * @package Plugin\jtl_example_payment_srv\src\Backend
 */
class Install
{
    /** @var PluginInterface */
    private $plugin;

    /** @var DbInterface */
    private $db;

    /**
     * Install constructor.
     * @param PluginInterface $plugin
     * @param DbInterface     $db
     */
    public function __construct(PluginInterface $plugin, DbInterface $db)
    {
        Shop::Container()->getGetText()->loadPluginLocale('install', $plugin);

        $this->plugin = $plugin;
        $this->db     = $db;
    }

    /**
     * @return void
     */
    public function install(): void
    {
        $alert = Shop::Container()->getAlertService();

        if (!$this->createTables()) {
            $alert->addAlert(Alert::TYPE_ERROR, __('Plugin tables cant be created'), 'createTables');

            return;
        }

        if (!($this->createMerchantData() && $this->createCustomertData())) {
            $alert->addAlert(Alert::TYPE_ERROR, __('Merchant and / or customer data cant be created'), 'createData');

            return;
        }
    }

    /**
     * @param bool $deleteData
     * @return void
     */
    public function uninstall($deleteData = true): void
    {
        $alert = Shop::Container()->getAlertService();

        if ($deleteData && !$this->dropTables()) {
            $alert->addAlert(Alert::TYPE_ERROR, __('Plugin tables cant be dropped'), 'dropTables');

            return;
        }
    }

    /**
     * @return bool
     */
    protected function createTables(): bool
    {
        return $this->db->query(
            'CREATE TABLE `xplugin_jtl_example_payment_srv_merchants` (
                `id`             int(11)         NOT NULL AUTO_INCREMENT,
                `merchant_name`  varchar(255)    NOT NULL,
                `merchant_id`    varchar(255)    NOT NULL,
                `secret`         varchar(255)    NOT NULL,
                `credit`         float           NOT NULL DEFAULT 0,
                PRIMARY KEY (`id`),
                UNIQUE KEY idx_token_uq (merchant_id)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1',
            ReturnType::DEFAULT
        ) && $this->db->query(
            'CREATE TABLE `xplugin_jtl_example_payment_srv_customers` (
                `id`             int(11)         NOT NULL AUTO_INCREMENT,
                `customer_name`  varchar(255)    NOT NULL,
                `secret`         varchar(255)    NOT NULL,
                `credit`         float           NOT NULL DEFAULT 0,
                PRIMARY KEY (`id`),
                UNIQUE KEY idx_customer_name_uq (customer_name)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1',
            ReturnType::DEFAULT
        ) && $this->db->query(
            'CREATE TABLE `xplugin_jtl_example_payment_srv_transactions` (
                `id`                int(11)         NOT NULL AUTO_INCREMENT,
                `transaction_name`  varchar(255)    NOT NULL,
                `merchant_id`       int(11)             NULL,
                `customer_id`       int(11)             NULL,
                `transaction_date`  datetime        NOT NULL,
                `transaction_value` float           NOT NULL DEFAULT 0,
                PRIMARY KEY (`id`),
                FOREIGN KEY (merchant_id) REFERENCES xplugin_jtl_example_payment_srv_merchants(id) ON DELETE CASCADE,
                FOREIGN KEY (customer_id) REFERENCES xplugin_jtl_example_payment_srv_customers(id) ON DELETE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1',
            ReturnType::DEFAULT
        ) && $this->db->query(
            'CREATE TABLE `xplugin_jtl_example_payment_srv_paymentrequests` (
                `id`                int(11)         NOT NULL AUTO_INCREMENT,
                `payment_key`       varchar(255)    NOT NULL,
                `transaction_name`  varchar(255)    NOT NULL,
                `merchant_id`       int(11)         NOT NULL,
                `customer_id`       int(11)         NOT NULL,
                `notification`      varchar(1024)       NULL,
                `transaction_date`  datetime        NOT NULL,
                `transaction_value` float           NOT NULL DEFAULT 0,
                PRIMARY KEY (`id`),
                FOREIGN KEY (merchant_id) REFERENCES xplugin_jtl_example_payment_srv_merchants(id) ON DELETE CASCADE,
                FOREIGN KEY (customer_id) REFERENCES xplugin_jtl_example_payment_srv_customers(id) ON DELETE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1',
            ReturnType::DEFAULT
        );
    }

    /**
     * @return bool
     */
    protected function dropTables(): bool
    {
        try {
            return $this->db->query(
                'DROP TABLE IF EXISTS `xplugin_jtl_example_payment_srv_paymentrequests`',
                ReturnType::DEFAULT
            ) && $this->db->query(
                'DROP TABLE IF EXISTS `xplugin_jtl_example_payment_srv_transactions`',
                ReturnType::DEFAULT
            ) && $this->db->query(
                'DROP TABLE IF EXISTS `xplugin_jtl_example_payment_srv_customers`',
                ReturnType::DEFAULT
            ) && $this->db->query(
                'DROP TABLE IF EXISTS `xplugin_jtl_example_payment_srv_merchants`',
                ReturnType::DEFAULT
            );
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @return bool
     */
    protected function createMerchantData(): bool
    {
        try {
            Merchant::instance()->assign([
                'merchant_name' => mb_substr(Shop::getConfigValue(1, 'global_shopname'), 0, 255),
            ])->save();

            return true;
        } catch (TransactionException $e) {
            return false;
        }
    }

    /**
     * @return bool
     */
    protected function createCustomertData(): bool
    {
        try {
            Customer::instance()->assign([
                'customer_name' => 'customer.test',
                'secret'        => 'customer'
            ])->save();

            return true;
        } catch (TransactionException $e) {
            return false;
        }
    }
}
