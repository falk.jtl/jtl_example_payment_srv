<?php
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 * @package       jtl-shop
 */

namespace Plugin\jtl_example_payment_srv\src\Backend;

use Illuminate\Support\Collection;
use JTL\Alert\Alert;
use JTL\DB\ReturnType;
use JTL\Plugin\PluginInterface;
use JTL\Shop;
use Plugin\jtl_example_payment_srv\src\Merchant;
use Plugin\jtl_example_payment_srv\src\TransactionException;

/**
 * Class Merchants
 * @package Plugin\jtl_example_payment_srv\src\Backend
 */
class Merchants
{
    /** @var PluginInterface */
    private $plugin;

    /**
     * Merchants constructor.
     * @param PluginInterface $plugin
     */
    public function __construct(PluginInterface $plugin)
    {
        $this->plugin = $plugin;
    }

    /**
     * @return Collection|null
     */
    protected function getMerchants(): ?Collection
    {
        $merchants = Shop::Container()->getDB()->query(
            'SELECT id, merchant_name, merchant_id, secret, credit
                FROM xplugin_jtl_example_payment_srv_merchants
                ORDER BY merchant_name',
            ReturnType::COLLECTION
        );

        return ($merchants instanceof Collection) ? $merchants : null;
    }

    /**
     * @param int $id
     * @return object|null
     */
    protected function getMerchant(int $id): ?object
    {
        $merchant = Shop::Container()->getDB()->queryPrepared(
            'SELECT id, merchant_name, merchant_id, secret, credit
                FROM xplugin_jtl_example_payment_srv_merchants
                WHERE id = :id',
            [
                'id' => $id,
            ],
            ReturnType::SINGLE_OBJECT
        );

        return \is_object($merchant) ? $merchant : null;
    }

    /**
     * @return object
     */
    protected static function emptyMerchant(): object
    {
        $cs = Shop::Container()->getCryptoService();

        try {
            return (object)[
                'merchant_name' => '',
                'merchant_id'   => $cs->randomString(12),
                'secret'        => $cs->randomString(32),
                'credit'        => 0,
            ];
        } catch (\Exception $e) {
            return (object)[
                'merchant_name' => '',
                'merchant_id'   => '',
                'secret'        => '',
                'credit'        => 0,
            ];
        }
    }

    /**
     * @param array $data
     * @return array
     */
    protected static function mapData(array $data): array
    {
        $data['merchant_name'] = $data['name'];
        $data['merchant_id']   = $data['key'];
        $data['credit']        = str_replace(['.', ','], ['', '.'], $data['credit']);

        return $data;
    }

    /**
     * @param int $selected
     * @return Collection
     */
    public function list(int $selected): Collection
    {
        return ($this->getMerchants() ?? new Collection())->map(static function ($item) use ($selected) {
            $item->selected = (int)$item->id === $selected;

            return $item;
        });
    }

    /**
     * @param int $id
     * @return void
     */
    public function willDelete(int $id): void
    {
        Shop::Smarty()->assign('merchant', $this->getMerchant($id));
    }

    /**
     * @param int $id
     * @return void
     */
    public function edit(int $id): void
    {
        Shop::Smarty()->assign('merchant', $id === 0 ? static::emptyMerchant() : $this->getMerchant($id));
    }

    /**
     * @param array $data
     * @return void
     */
    public function save(array $data): void
    {
        try {
            Merchant::instance()->assign(static::mapData($data))->save();
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_SUCCESS,
                __('Merchant successfully saved'),
                'merchantSave',
                ['saveInSession' => true]
            );
        } catch (TransactionException $e) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                __($e->getMessage()),
                'merchantSave',
                ['saveInSession' => true]
            );
        }
    }

    /**
     * @param int $id
     * @return void
     */
    public function delete(int $id): void
    {
        try {
            Merchant::instance()->assign(['id' => $id])->delete();
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_SUCCESS,
                __('Merchant successfully deleted'),
                'merchantDeleted',
                ['saveInSession' => true]
            );
        } catch (TransactionException $e) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                __($e->getMessage()),
                'merchantSave',
                ['saveInSession' => true]
            );
        }
    }

    /**
     * @param int $id
     * @return void
     */
    public function refresh(int $id): void
    {
        try {
            Merchant::instance()->assign(['id' => $id])->refresh();
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_SUCCESS,
                __('Merchant successfully refreshed'),
                'merchantRefreshed',
                ['saveInSession' => true]
            );
        } catch (TransactionException $e) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                __($e->getMessage()),
                'merchantRefreshed',
                ['saveInSession' => true]
            );
        }
    }

    /**
     * @return void
     */
    public function overview(): void
    {
        Shop::Smarty()->assign('merchants', $this->getMerchants());
    }
}
