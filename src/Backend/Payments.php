<?php
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 * @package       jtl-shop
 */

namespace Plugin\jtl_example_payment_srv\src\Backend;

use JTL\Alert\Alert;
use JTL\DB\ReturnType;
use JTL\Shop;
use Plugin\jtl_example_payment_srv\src\Payment;
use Plugin\jtl_example_payment_srv\src\TransactionException;

/**
 * Class Payments
 * @package Plugin\jtl_example_payment_srv\src\Backend
 */
class Payments extends AbstractTransaction
{
    /**
     * @inheritDoc
     */
    protected function getContext(): string
    {
        return 'payments';
    }

    /**
     * @inheritDoc
     */
    protected static function emptyTransaction(): object
    {
        $payment = parent::emptyTransaction();
        $cs      = Shop::Container()->getCryptoService();

        try {
            $payment->payment_key = $cs->randomString(32);
        } catch (\Exception $e) {
            $payment->payment_key = '';
        }

        $payment->notification = '';

        return $payment;
    }

    /**
     * @inheritDoc
     */
    protected function queryTransactions(array $filter, array $params)
    {
        return Shop::Container()->getDB()->queryPrepared(
            'SELECT SQL_CALC_FOUND_ROWS t.id, t.payment_key, t.transaction_name, t.notification,
                t.transaction_date, t.transaction_value,
                t.merchant_id, m.merchant_name,
                t.customer_id, c.customer_name
                FROM xplugin_jtl_example_payment_srv_paymentrequests t
                LEFT JOIN xplugin_jtl_example_payment_srv_merchants m ON m.id = t.merchant_id
                LEFT JOIN xplugin_jtl_example_payment_srv_customers c ON c.id = t.customer_id
                ' . (count($filter) > 0 ? 'WHERE ' . implode(' AND ', $filter) : '') . '
                ORDER BY t.transaction_date DESC'
            . (!empty($this->limitStr) ? ' LIMIT ' . $this->limitStr : ''),
            $params,
            ReturnType::COLLECTION
        );
    }

    /**
     * @inheritDoc
     */
    protected function queryTransaction(int $id)
    {
        return Shop::Container()->getDB()->queryPrepared(
            'SELECT t.id, t.payment_key, t.transaction_name, t.notification,
                t.transaction_date, t.transaction_value,
                t.merchant_id, m.merchant_name,
                t.customer_id, c.customer_name
                FROM xplugin_jtl_example_payment_srv_paymentrequests t
                LEFT JOIN xplugin_jtl_example_payment_srv_merchants m ON m.id = t.merchant_id
                LEFT JOIN xplugin_jtl_example_payment_srv_customers c ON c.id = t.customer_id
                WHERE t.id = :id',
            [
                'id' => $id,
            ],
            ReturnType::SINGLE_OBJECT
        );
    }

    /**
     * @inheritDoc
     */
    public function save(array $data): void
    {
        try {
            Payment::instance()->assign(static::mapData($data))->save();
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_SUCCESS,
                __('Payment successfully saved'),
                'paymentSave',
                ['saveInSession' => true]
            );
        } catch (TransactionException $e) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                __($e->getMessage()),
                'paymentSave',
                ['saveInSession' => true]
            );
        }
    }

    /**
     * @inheritDoc
     */
    public function delete(int $id): void
    {
        $payment = Payment::instance()->assign(['id' => $id]);
        try {
            $payment->delete();
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_SUCCESS,
                __('Payment successfully deleted'),
                'paymentDelete',
                ['saveInSession' => true]
            );
        } catch (TransactionException $e) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                __($e->getMessage()),
                'paymentDelete',
                ['saveInSession' => true]
            );
        }
    }

    /**
     * @param int $id
     * @return void
     */
    public function accept(int $id): void
    {
        try {
            Payment::instance()->assign(['id' => $id])->accept();
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_SUCCESS,
                __('Payment successfully accepted'),
                'paymentAccepted',
                ['saveInSession' => true]
            );
        } catch (TransactionException $e) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                __($e->getMessage()),
                'paymentAccepted',
                ['saveInSession' => true]
            );
        }
    }
}
