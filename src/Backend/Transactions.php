<?php
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 * @package       jtl-shop
 */

namespace Plugin\jtl_example_payment_srv\src\Backend;

use Illuminate\Support\Collection;
use JTL\Alert\Alert;
use JTL\DB\ReturnType;
use JTL\Shop;
use Plugin\jtl_example_payment_srv\src\Transaction;
use Plugin\jtl_example_payment_srv\src\TransactionException;

/**
 * Class Transactions
 * @package Plugin\jtl_example_payment_srv\src\Backend
 */
class Transactions extends AbstractTransaction
{
    /**
     * @inheritDoc
     */
    protected function getContext(): string
    {
        return 'transactions';
    }

    /**
     * @param array $filter
     * @param array $params
     * @return array|int|object|Collection
     */
    protected function queryTransactions(array $filter, array $params)
    {
        return Shop::Container()->getDB()->queryPrepared(
            'SELECT SQL_CALC_FOUND_ROWS t.id, t.transaction_name, t.transaction_date, t.transaction_value,
                t.merchant_id, m.merchant_name,
                t.customer_id, c.customer_name
                FROM xplugin_jtl_example_payment_srv_transactions t
                LEFT JOIN xplugin_jtl_example_payment_srv_merchants m ON m.id = t.merchant_id
                LEFT JOIN xplugin_jtl_example_payment_srv_customers c ON c.id = t.customer_id
                ' . (count($filter) > 0 ? 'WHERE ' . implode(' AND ', $filter) : '') . '
                ORDER BY t.transaction_date DESC'
            . (!empty($this->limitStr) ? ' LIMIT ' . $this->limitStr : ''),
            $params,
            ReturnType::COLLECTION
        );
    }

    /**
     * @param int $id
     * @return array|int|object
     */
    protected function queryTransaction(int $id)
    {
        return Shop::Container()->getDB()->queryPrepared(
            'SELECT t.id, t.transaction_name, t.transaction_date, t.transaction_value,
                t.merchant_id, m.merchant_name,
                t.customer_id, c.customer_name
                FROM xplugin_jtl_example_payment_srv_transactions t
                LEFT JOIN xplugin_jtl_example_payment_srv_merchants m ON m.id = t.merchant_id
                LEFT JOIN xplugin_jtl_example_payment_srv_customers c ON c.id = t.customer_id
                WHERE t.id = :id',
            [
                'id' => $id,
            ],
            ReturnType::SINGLE_OBJECT
        );
    }

    /**
     * @param array $data
     * @return void
     */
    public function save(array $data): void
    {
        try {
            Transaction::instance()->assign(static::mapData($data))->save();
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_SUCCESS,
                __('Transaction successfully saved'),
                'transactionSave',
                ['saveInSession' => true]
            );
        } catch (TransactionException $e) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                __($e->getMessage()),
                'transactionSave',
                ['saveInSession' => true]
            );
        }
    }

    /**
     * @param int $id
     * @return void
     */
    public function delete(int $id): void
    {
        $transaction = $this->getTransaction($id);

        if (Shop::Container()->getDB()->delete('xplugin_jtl_example_payment_srv_transactions', 'id', $id)) {
            if ($transaction !== null) {
                if ((int)$transaction->merchant_id > 0) {
                    (new Merchants($this->getPlugin()))->refresh((int)$transaction->merchant_id);
                }
                if ((int)$transaction->customer_id > 0) {
                    (new Customers($this->getPlugin()))->refresh((int)$transaction->customer_id);
                }
            }
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_SUCCESS,
                __('Transaction successfully deleted'),
                'transactionDeleted',
                ['saveInSession' => true]
            );
        } else {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                __('Transaction cant be deleted'),
                'transactionDeleted',
                ['saveInSession' => true]
            );
        }
    }
}
