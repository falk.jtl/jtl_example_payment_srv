<?php
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 * @package       jtl-shop
 */

namespace Plugin\jtl_example_payment_srv\src;

use JTL\DB\ReturnType;
use JTL\Shop;

/**
 * Class Customer
 * @package Plugin\jtl_example_payment_srv\src
 */
class Customer extends TransactionMember
{
    /** @var string */
    public $customer_name;

    /**
     * Customer constructor.
     */
    protected function __construct()
    {
        parent::__construct();
        $this->table = 'xplugin_jtl_example_payment_srv_customers';
    }

    /**
     * @inheritDoc
     */
    public function assign($data)
    {
        parent::assign($data);

        if ($data === null) {
            return $this;
        }

        if (\is_array($data)) {
            $data = ((object)$data);
        }

        $this->customer_name = $data->customer_name ?? null;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getData(): object
    {
        $data = parent::getData();

        $data->customer_name = $this->customer_name;
        if (!empty($data->id) && empty($this->secret)) {
            unset($data->secret);
        } else {
            $ps     = Shop::Container()->getPasswordService();
            $pwInfo = $ps->getInfo($this->secret);
            if (!array($pwInfo) || empty($pwInfo['algo'])) {
                $pw = $data->secret;
                try {
                    $data->secret = $ps->hash($this->secret);
                } catch (\Exception $e) {
                    $data->secret = $pw;
                }
            } else {
                $data->secret = $this->secret;
            }
        }

        return  $data;
    }

    /**
     * @inheritDoc
     */
    protected function postProcessSave(bool $result): void
    {
        if (!$result) {
            throw new TransactionException('Customer cant be saved');
        }

        Transaction::instance()->correctCustomerTransaction($this->id, $this->credit);
    }

    /**
     * @inheritDoc
     */
    protected function postProcessDelete(bool $result): void
    {
        if (!$result) {
            throw new TransactionException('Customer cant be deleted');
        }
    }

    /**
     * @inheritDoc
     */
    public function refresh()
    {
        $updated = Shop::Container()->getDB()->queryPrepared(
            'UPDATE xplugin_jtl_example_payment_srv_customers SET
                credit = ROUND(
                    (SELECT -SUM(xplugin_jtl_example_payment_srv_transactions.transaction_value)
                        FROM xplugin_jtl_example_payment_srv_transactions
                        WHERE xplugin_jtl_example_payment_srv_transactions.customer_id = :customerID
                    ), 4)
                WHERE xplugin_jtl_example_payment_srv_customers.id = :customerID',
            [
                'customerID' => $this->id,
            ],
            ReturnType::DEFAULT
        );

        if ($updated !== true) {
            throw new TransactionException('Customer cant be refreshed');
        }

        return $this;
    }
}
