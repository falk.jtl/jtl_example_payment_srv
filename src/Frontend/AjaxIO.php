<?php
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 * @package       jtl-shop
 */

namespace Plugin\jtl_example_payment_srv\src\Frontend;

/**
 * Class AjaxIO
 * @package Plugin\jtl_example_payment_srv\src\Frontend
 */
class AjaxIO
{
    public const ENTRYKEY   = 'example_payment_srv';
    public const ENTRYPOINT = 'handleRequest';

    public static function handleRequest(string $method, string $jsonData): object
    {
        if (\is_callable([self::class, $method])) {
            return \call_user_func_array([new static(), $method], json_decode($jsonData, true));
        }

        return (object)[
            'result'  => 'failure',
            'message' => 'method ' . $method . ' not found',
            'data'    => json_decode($jsonData, false),
        ];
    }

    public function test(): object
    {
        return new \stdClass();
    }
}
