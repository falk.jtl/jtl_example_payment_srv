<?php
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 * @package       jtl-shop
 */

namespace Plugin\jtl_example_payment_srv\src\Frontend;

use Exception;
use JTL\Alert\Alert;
use JTL\Helpers\Form;
use JTL\Helpers\Request;
use JTL\Plugin\PluginInterface;
use JTL\Session\Frontend;
use JTL\Shop;
use ReflectionMethod;
use StringHandler;

/**
 * Class FrontendProvider
 * @package Plugin\jtl_example_payment_srv\src\Frontend
 */
class FrontendProvider
{
    /** @var PluginInterface */
    private $plugin;

    protected const USERDATA = 'jtl_example_payment_srv.userdata';

    /**
     * FrontendProvider constructor.
     * @param PluginInterface $plugin
     */
    public function __construct(PluginInterface $plugin)
    {
        $this->plugin = $plugin;
    }

    /**
     * @param string $task
     * @return void
     * @uses paymentFrontend
     */
    public function run(string $task): void
    {
        $params = StringHandler::filterXSS(array_merge(
            json_decode(Request::getVar('params', '{}'), true),
            json_decode(Request::postVar('params', '{}'), true),
            $_POST
        ));

        if (!\is_callable([$this, $task])) {
            $this->error('method not implemented!', 'not_implemented', [$task]);
            $this->display();

            return;
        }

        try {
            $ref = new ReflectionMethod($this, $task);
        } catch (\ReflectionException $e) {
            $this->error($e->getMessage(), 'internal');
            $this->display();

            return;
        }

        if (!$ref->isPublic()) {
            $this->error('method not callable!', 'not_callable', [$task]);
            $this->display();

            return;
        }

        if ($ref->getNumberOfRequiredParameters() > \count($params)) {
            $this->error('wrong required parameter count', 'wrong_parameter');
            $this->display();

            return;
        }

        try {
            \call_user_func_array([$this, $task], $params);
        } catch (Exception $e) {
            $this->error($e->getMessage(), $task);
            $this->display();

            return;
        }

        $this->display();
    }

    /**
     * @param string $errMsg
     * @param string $key
     * @param array  $params
     * @return void
     */
    protected function error(string $errMsg, string $key, array $params = []): void
    {
        Shop::Container()->getAlertService()->addAlert(
            Alert::TYPE_ERROR,
            sprintf(__($errMsg), $params),
            $key
        );
    }

    /**
     * @return void
     */
    protected function display(): void
    {
        $_GET['exclusive_content'] = 1;
        Shop::Smarty()
            ->assign('plugin', $this->plugin)
            ->assign('bExclusive', true)
            ->assign('pluginLocale', $this->plugin->getLocalization());
    }

    /**
     * @param string $task
     * @param array  $data
     * @return void
     * @throws Exception
     */
    protected function frontendLogin(string $task, array $data): void
    {
        $token = (new ServiceProvider(false))->checkCustomer($data['customer'], $data['secret']);
        Shop::Smarty()
            ->assign('task', 'sendBack')
            ->assign('context', $data['context'])
            ->assign('response', htmlentities(json_encode((object)[
                'task'   => $task,
                'result' => 'ok',
                'token'  => $token
            ])))
            ->assign('returnURL', $data['returnURL']);
    }

    /**
     * @param string $task
     * @param array  $data
     * @return void
     */
    protected function frontendPayTransaction(string $task, array $data): void
    {
        try {
            $result = (object)[
                'task'   => $task,
                'result' => 'ok',
                'token'  => (new ServiceProvider(false))->payTransaction(
                    $data['transactionName'],
                    $data['merchantID'],
                    $data['customerHash'],
                    $data['notification'] ?? '',
                    $data['value'],
                    $data['token']
                )
            ];
        } catch (Exception $e) {
            $result = (object)[
                'task'   => $task,
                'result' => 'failed',
                'msg'    => $e->getMessage(),
                'token'  => ''
            ];
        }
        Shop::Smarty()
            ->assign('task', 'sendBack')
            ->assign('context', $data['context'])
            ->assign('response', htmlentities(json_encode($result)))
            ->assign('returnURL', $data['returnURL']);
    }

    /**
     * @param string $task
     * @param array  $data
     * @return void
     */
    protected function frontendLoginForm(string $task, array $data): void
    {
        $param  = $data['context'] . '=cancel';
        $retURL = urldecode($data['returnURL']);
        Shop::Smarty()
            ->assign('task', $task)
            ->assign('credentials_loginName', empty($data['customer']) ? '' : $data['customer'])
            ->assign('credentials_secret', '')
            ->assign('cancelURL', $retURL . (strpos($retURL, '?') === false ? '?' : '&') . $param);
    }

    /**
     * @param array $post
     * @return void
     */
    public function paymentFrontend(array $post): void
    {
        $data   = array_merge(Frontend::get(self::USERDATA, []), $post);
        $task   = empty($data['task']) ? 'form' : $data['task'];
        $smarty = Shop::Smarty();

        if (empty($data['returnURL'])) {
            $this->error('invalid return url', 'invalidURL');
            $smarty->assign('task', 'error');

            return;
        }
        if (isset($data['post']) && !Form::validateToken()) {
            $this->error(Shop::Lang()->getTranslation('invalidToken'), 'invalidToken');
            $task = 'form';
        }
        if (empty($data['context'])) {
            $data['context'] = $this->plugin->getPluginID();
        }

        switch ($task) {
            case 'login':
                try {
                    $this->frontendLogin($task, $data);
                } catch (Exception $e) {
                    unset($post['secret'], $post['post']);
                    $post['task'] = 'form';
                    $this->error($e->getMessage(), 'invalidUser');
                    $this->paymentFrontend($post);

                    return;
                }

                break;
            case 'payTransaction':
                $this->frontendPayTransaction($task, $data);
                break;
            case 'form':
            default:
                $this->frontendLoginForm($task, $data);
                break;
        }

        unset($data['post'], $data['task'], $data['secret']);
        Frontend::set(self::USERDATA, $data);
    }
}
