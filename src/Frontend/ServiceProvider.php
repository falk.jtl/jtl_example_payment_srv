<?php
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 * @package       jtl-shop
 */

namespace Plugin\jtl_example_payment_srv\src\Frontend;

use Exception;
use JTL\DB\ReturnType;
use JTL\Helpers\Request;
use JTL\Session\Frontend;
use JTL\Shop;
use Plugin\jtl_example_payment_srv\src\Customer;
use Plugin\jtl_example_payment_srv\src\Merchant;
use Plugin\jtl_example_payment_srv\src\Payment;
use ReflectionMethod;
use StringHandler;

/**
 * Class ServiceProvider
 * @package Plugin\jtl_example_payment_srv\src\Frontend
 */
class ServiceProvider
{
    /** @var bool */
    private $asService;

    public const UNAUTHORIZED   = 401;
    public const FORBIDDEN      = 403;
    public const NOT_FOUND      = 404;
    public const NOT_ALLOWED    = 405;
    public const BAD_REQUEST    = 400;
    public const INTERNAL_ERROR = 500;

    protected const USERTOKENS = 'jtl_example_payment_srv.usertokens';

    /**
     * ServiceProvider constructor.
     * @param bool $asService
     */
    public function __construct($asService = true)
    {
        $this->asService = $asService;
    }

    /**
     * @return void
     * @throws Exception
     * @uses payTransaction()
     * @uses payPrepaidTransaction()
     * @uses checkCustomer()
     * @uses checkCustomerCredit()
     */
    public function run(): void
    {
        register_shutdown_function([__CLASS__, 'fatal']);
        ini_set('display_errors', 0);
        ini_set('error_reporting', E_CORE_ERROR | E_USER_ERROR | E_ERROR);

        $method = StringHandler::filterXSS(Request::getVar('method'));
        $params = json_decode(Request::postVar('params', '{}'), true);
        #$params = json_decode(Request::getVar('params', '{}'), true);

        if ($method === null || !\is_callable([$this, $method])) {
            $this->error(self::NOT_ALLOWED, 'method ' . $method . ' not implemented!');
        }

        try {
            $ref = new ReflectionMethod($this, $method);
        } catch (\ReflectionException $e) {
            $this->error(self::BAD_REQUEST, $e->getMessage());
        }

        if (!$ref->isPublic()) {
            $this->error(self::NOT_ALLOWED, 'method ' . $method . ' not callable!');
        }

        if ($ref->getNumberOfRequiredParameters() > \count($params)) {
            $this->error(self::BAD_REQUEST, 'wrong required parameter count');
        }

        try {
            $this->response(\call_user_func_array([$this, $method], $params));
        } catch (Exception $e) {
            $this->error(self::INTERNAL_ERROR, $e->getMessage());
        }
    }

    /**
     * @param int    $errCode
     * @param string $errMsg
     * @return void
     * @throws Exception
     */
    protected function error(int $errCode, string $errMsg): void
    {
        if ($this->asService) {
            $errObj = (object)[
                'result'  => 'failure',
                'message' => (object)[
                    'code' => $errCode,
                    'text' => $errMsg,
                ]
            ];

            exit(json_encode($errObj));
        }

        throw new Exception($errMsg, $errCode);
    }

    /**
     * @return void
     */
    public static function fatal(): void
    {
        $error = error_get_last();
        if ($error !== null
            && ($error['type'] === E_ERROR || $error['type'] === E_CORE_ERROR || $error['type'] === E_USER_ERROR)
        ) {
            ob_clean();
            $errObj = (object)[
                'result'  => 'fatal error',
                'message' => (object)[
                    'code' => 500,
                    'text' => $error['message'],
                ]
            ];

            exit(json_encode($errObj));
        }
    }

    /**
     * @param mixed $data
     * @return void
     */
    protected function response($data): void
    {
        $obj = (object)[
            'result'   => 'ok',
            'response' => $data,
        ];

        exit(json_encode($obj));
    }

    /**
     * @param string $customerHash
     * @return Customer
     * @throws Exception
     */
    protected function validateCustomerHash(string $customerHash): Customer
    {
        $tokens    = Frontend::get(self::USERTOKENS, []);
        $hashParts = explode('.', $customerHash, 2);
        $customer  = Shop::Container()->getDB()->queryPrepared(
            'SELECT id
                FROM xplugin_jtl_example_payment_srv_customers
                WHERE customer_name = :customerName',
            [
                'customerName' => base64_decode($hashParts[0]),
            ],
            ReturnType::SINGLE_OBJECT
        );

        if (!\is_object($customer)
            || empty($tokens[(int)$customer->id])
            || $tokens[(int)$customer->id] !== $hashParts[1]
        ) {
            $this->error(self::UNAUTHORIZED, 'unknown token');
        }

        return Customer::instance($customer->id);
    }

    /**
     * @param string $merchantID
     * @return Merchant
     * @throws Exception
     */
    protected function validateMerchant(string $merchantID): Merchant
    {
        $merchant = Shop::Container()->getDB()->queryPrepared(
            'SELECT id
                FROM xplugin_jtl_example_payment_srv_merchants
                WHERE merchant_id = :merchantID',
            [
                'merchantID' => $merchantID,
            ],
            ReturnType::SINGLE_OBJECT
        );

        if (!\is_object($merchant)) {
            $this->error(self::NOT_FOUND, 'unknown merchant');
        }

        return Merchant::instance($merchant->id);
    }

    /**
     * @param array  $data
     * @param string $token
     * @return bool
     * @throws Exception
     */
    protected function validateToken(array $data, string $token): bool
    {
        $chkToken = sha1(serialize($data));

        if ($chkToken !== $token) {
            $this->error(self::BAD_REQUEST, 'data is inconsistent');
        }

        return true;
    }

    /**
     * @param string $customerName
     * @param string $secret
     * @return string
     * @throws Exception
     */
    public function checkCustomer(string $customerName, string $secret): string
    {
        $ps       = Shop::Container()->getPasswordService();
        $customer = Shop::Container()->getDB()->queryPrepared(
            'SELECT id, customer_name, secret
                FROM xplugin_jtl_example_payment_srv_customers
                WHERE customer_name = :customerName',
            [
                'customerName' => $customerName,
            ],
            ReturnType::SINGLE_OBJECT
        );

        if (!\is_object($customer)
            || $customer->customer_name !== $customerName
            || !$ps->verify($secret, $customer->secret)
        ) {
            $this->error(self::FORBIDDEN, 'password or username failed');
        }

        $tokens = Frontend::get(self::USERTOKENS, []);
        try {
            $token = Shop::Container()->getCryptoService()->randomString(16);
        } catch (Exception $e) {
            $token = \uniqid('', false);
        }

        $tokens[(int)$customer->id] = $token;
        Frontend::set(self::USERTOKENS, $tokens);

        return base64_encode($customer->customer_name). '.' . $token;
    }

    /**
     * @param string $customerHash
     * @param float  $credit
     * @return bool
     * @throws Exception
     */
    public function checkCustomerCredit(string $customerHash, float $credit): bool
    {
        $customer = $this->validateCustomerHash($customerHash);

        return (float)$customer->credit >= $credit;
    }

    /**
     * @param string $transactionName
     * @param string $merchantID
     * @param string $customerHash
     * @param string $notification
     * @param float  $value
     * @param string $token
     * @param bool   $autoAccept
     * @return string
     * @throws Exception
     */
    public function payTransaction(
        string $transactionName,
        string $merchantID,
        string $customerHash,
        string $notification,
        float $value,
        string $token,
        bool $autoAccept = false
    ): string {
        $customer = $this->validateCustomerHash($customerHash);
        $merchant = $this->validateMerchant($merchantID);
        $this->validateToken([$transactionName, $merchantID, $merchant->secret, $customerHash, $value], $token);

        $payment = Payment::instance()->assign([
            'transaction_name'  => $transactionName,
            'merchant_id'       => $merchant->id,
            'customer_id'       => $customer->id,
            'notification'      => $notification,
            'transaction_value' => $value,
        ]);
        $payment->save()->load($payment->id);

        if ($autoAccept && (float)$customer->credit >= $value) {
            $payment->accept();
        }

        return $payment->payment_key;
    }

    /**
     * @param string $transactionName
     * @param string $merchantID
     * @param string $customerHash
     * @param string $notification
     * @param float  $value
     * @param string $token
     * @param bool   $force
     * @return object
     * @throws Exception
     */
    public function payPrepaidTransaction(
        string $transactionName,
        string $merchantID,
        string $customerHash,
        string $notification,
        float $value,
        string $token,
        bool $force = false
    ): object {
        $customer = $this->validateCustomerHash($customerHash);
        $merchant = $this->validateMerchant($merchantID);
        $this->validateToken([$transactionName, $merchantID, $merchant->secret, $customerHash, $value], $token);

        if ($force) {
            /** @noinspection CallableParameterUseCaseInTypeContextInspection */
            $value = \round(\max(\min($customer->refresh()->load($customer->id)->credit, $value), 0), 4);
        }

        if ($value > 0 && $this->checkCustomerCredit($customerHash, $value)) {
            $payment = Payment::instance()->assign([
                'transaction_name'  => $transactionName,
                'merchant_id'       => $merchant->id,
                'customer_id'       => $customer->id,
                'notification'      => $notification,
                'transaction_value' => $value,
            ]);
            $payment->save()->load($payment->id)->accept();
        } else {
            $this->error(self::BAD_REQUEST, 'not enough credit');
        }

        return (object)[
            'payment_key'   => $payment->payment_key,
            'payment_value' => $value,
        ];
    }
}
