<?php
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 * @package       jtl-shop
 */

namespace Plugin\jtl_example_payment_srv\src;

use JTL\DB\ReturnType;
use JTL\Shop;

/**
 * Class Merchant
 * @package Plugin\jtl_example_payment_srv\src
 */
class Merchant extends TransactionMember
{
    /** @var string */
    public $merchant_name;

    /** @var string */
    public $merchant_id;

    /**
     * Merchant constructor.
     */
    protected function __construct()
    {
        parent::__construct();
        $this->table = 'xplugin_jtl_example_payment_srv_merchants';
    }

    /**
     * @inheritDoc
     */
    public function assign($data)
    {
        parent::assign($data);

        if ($data === null) {
            return $this;
        }

        $cs = Shop::Container()->getCryptoService();
        if (\is_array($data)) {
            $data = ((object)$data);
        }

        $this->merchant_name = $data->merchant_name ?? null;
        try {
            $this->merchant_id = $data->merchant_id ?? $cs->randomString(12);
        } catch (\Exception $e) {
            $this->merchant_id = uniqid('', false);
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getData(): object
    {
        $cs   = Shop::Container()->getCryptoService();
        $data = parent::getData();

        $data->merchant_name = $this->merchant_name;
        try {
            $data->merchant_id = empty($this->merchant_id) ? $cs->randomString(12) : $this->merchant_id;
            $data->secret      = empty($this->secret) ? $cs->randomString(32) : $this->secret;
        } catch (\Exception $e) {
            $data->merchant_id = uniqid('', false);
            $data->secret      = uniqid('merchant.', true);
        }

        return  $data;
    }

    /**
     * @inheritDoc
     */
    protected function postProcessSave(bool $result): void
    {
        if (!$result) {
            throw new TransactionException('Merchant cant be saved');
        }

        Transaction::instance()->correctMerchantTransaction($this->id, $this->credit);
    }

    /**
     * @inheritDoc
     */
    protected function postProcessDelete(bool $result): void
    {
        if (!$result) {
            throw new TransactionException('Merchant cant be deleted');
        }
    }

    /**
     * @inheritDoc
     */
    public function refresh()
    {
        if (Shop::Container()->getDB()->queryPrepared(
            'UPDATE xplugin_jtl_example_payment_srv_merchants SET
                credit = ROUND(
                    (SELECT SUM(xplugin_jtl_example_payment_srv_transactions.transaction_value)
                        FROM xplugin_jtl_example_payment_srv_transactions
                        WHERE xplugin_jtl_example_payment_srv_transactions.merchant_id = :merchantID
                    ), 4)
                WHERE xplugin_jtl_example_payment_srv_merchants.id = :merchantID',
            [
                'merchantID' => $this->id,
            ],
            ReturnType::DEFAULT
        ) !== true) {
            throw new TransactionException('Customer cant be refreshed');
        }

        return $this;
    }
}
