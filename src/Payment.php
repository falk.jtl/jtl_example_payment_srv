<?php /** @noinspection ReturnTypeCanBeDeclaredInspection */

/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 * @package       jtl-shop
 */

namespace Plugin\jtl_example_payment_srv\src;

use JTL\Helpers\Request;
use JTL\Shop;

/**
 * Class Payment
 * @package Plugin\jtl_example_payment_srv\src
 */
class Payment extends Transaction
{
    /** @var string */
    public $payment_key;

    /** @var string */
    public $notification;

    /**
     * Payment constructor.
     */
    protected function __construct()
    {
        parent::__construct();

        $this->table = 'xplugin_jtl_example_payment_srv_paymentrequests';
    }

    /**
     * @inheritDoc
     */
    public function assign($data)
    {
        parent::assign($data);

        $this->notification = $this->notification === '_DBNULL_' ? null : $this->notification;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getData(): object
    {
        $data = parent::getData();
        $cs   = Shop::Container()->getCryptoService();

        if (empty($this->payment_key)) {
            try {
                $data->payment_key = $cs->randomString(32);
            } catch (\Exception $e) {
                $data->payment_key = uniqid('', false);
            }
        } else {
            $data->payment_key = $this->payment_key;
        }

        $data->notification = empty($this->notification) ? '_DBNULL_' : $this->notification;

        return $data;
    }

    /**
     * @inheritDoc
     */
    protected function postProcessSave($old, bool $result): void
    {
        if (!$result) {
            throw new TransactionException('Payment cant be saved');
        }
    }

    /**
     * @inheritDoc
     */
    protected function postProcessDelete($old, bool $result): void
    {
        if (!$result) {
            throw new TransactionException('Payment cant be deleted');
        }
        /** @var Payment $old */
        $old->callNotification('deletePayment');
    }

    /**
     * @param string $context
     * @return static
     * @throws TransactionException
     */
    protected function callNotification(string $context)
    {
        if (empty($this->notification)) {
            return $this;
        }
        $postData = [$context => [
            'payment_value' => $this->transaction_value,
            'payment_key'   => $this->payment_key,
            'payer_name'    => Customer::instance($this->customer_id)->customer_name,
        ]];
        $status   = Request::make_http_request($this->notification, 5, http_build_query($postData), true);
        if ($status < 200 || $status >= 300) {
            throw new TransactionException('Notification can not be sent');
        }

        return $this;
    }

    /**
     * @return void
     * @throws TransactionException
     */
    public function accept(): void
    {
        $this->load($this->id)->callNotification('acceptPayment')->delete();
        try {
            Transaction::instance()->assign([
                'transaction_name'  => $this->transaction_name,
                'merchant_id'       => $this->merchant_id,
                'customer_id'       => $this->customer_id,
                'transaction_date'  => $this->transaction_date,
                'transaction_value' => $this->transaction_value
            ])->save();
        } catch (TransactionException $e) {
            $this->save();
        }
    }
}
