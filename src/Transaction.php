<?php /** @noinspection ReturnTypeCanBeDeclaredInspection */

/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 * @package       jtl-shop
 */

namespace Plugin\jtl_example_payment_srv\src;

use DateTime;
use Exception;
use JTL\DB\ReturnType;
use JTL\Shop;

/**
 * Class Transaction
 * @package Plugin\jtl_example_payment_srv\src
 */
class Transaction
{
    /** @var int */
    public $id;

    /** @var string */
    public $transaction_name;

    /** @var int  */
    public $merchant_id;

    /** @var int */
    public $customer_id;

    /** @var DateTime */
    public $transaction_date;

    /** @var float */
    public $transaction_value;

    /** @var string */
    protected $table;

    /**
     * Transaction constructor.
     */
    protected function __construct()
    {
        $this->table = 'xplugin_jtl_example_payment_srv_transactions';
    }

    /**
     * @param int|null $id
     * @return static
     */
    public static function instance(?int $id = null)
    {
        $instance = new static();

        if ($id !== null) {
            $instance->load($id);
        }

        return $instance;
    }

    /**
     * @return static
     */
    public function reset()
    {
        foreach (array_keys(get_object_vars($this)) as $var) {
            $this->$var = null;
        }

        return $this;
    }

    /**
     * @param array|object|null $data
     * @return static
     */
    public function assign($data)
    {
        if ($data === null) {
            return $this->reset();
        }

        if (\is_array($data)) {
            $data = ((object)$data);
        }

        foreach (array_keys(get_object_vars($this)) as $var) {
            if (isset($data->$var)) {
                $this->$var = $data->$var;
            }
        }

        $this->id                = (int)$this->id;
        $this->customer_id       = $this->customer_id === '_DBNULL_' ? null : (int)$this->customer_id;
        $this->merchant_id       = $this->merchant_id === '_DBNULL_' ? null : (int)$this->merchant_id;
        $this->transaction_value = (float)$this->transaction_value;

        if ($this->transaction_date !== null && !is_a($this->transaction_date, DateTime::class)) {
            try {
                $this->transaction_date = new DateTime($this->transaction_date);
            } catch (Exception $e) {
                try {
                    $this->transaction_date = new DateTime();
                } catch (Exception $e) {
                    $this->transaction_date = null;
                }
            }
        }

        return $this;
    }

    /**
     * @return object
     */
    public function getData(): object
    {
        return (object)[
            'id'                => $this->id,
            'transaction_name'  => empty($this->transaction_name) ? '' : $this->transaction_name,
            'merchant_id'       => empty($this->merchant_id) ? '_DBNULL_' : $this->merchant_id,
            'customer_id'       => empty($this->customer_id) ? '_DBNULL_' : $this->customer_id,
            'transaction_date'  => $this->transaction_date === null
                ? 'NOW()'
                : $this->transaction_date->format('Y-m-d H:i:s'),
            'transaction_value' => \round($this->transaction_value, 4),
        ];
    }

    /**
     * @param int $id
     * @return static
     */
    public function load(int $id)
    {
        return $this->assign(
            Shop::Container()->getDB()->selectSingleRow($this->table, 'id', $id)
        );
    }

    /**
     * @param static $old
     * @param bool   $result
     * @return void
     * @throws TransactionException
     */
    protected function postProcessSave($old, bool $result): void
    {
        if (!$result) {
            throw new TransactionException('Transaction cant be saved');
        }

        if (!empty($this->merchant_id)) {
            Merchant::instance($this->merchant_id)->refresh();
        }
        if (!empty($this->customer_id)) {
            Customer::instance($this->customer_id)->refresh();
        }
        if (!empty($old->merchant_id)) {
            Merchant::instance($old->merchant_id)->refresh();
        }
        if (!empty($old->customer_id)) {
            Customer::instance($old->customer_id)->refresh();
        }
    }

    /**
     * @param static $old
     * @param bool   $result
     * @return void
     * @throws TransactionException
     */
    protected function postProcessDelete($old, bool $result): void
    {
        if (!$result) {
            throw new TransactionException('Transaction cant be deleted');
        }
        if (!empty($this->merchant_id)) {
            Merchant::instance($this->merchant_id)->refresh();
        }
        if (!empty($this->customer_id)) {
            Customer::instance($this->customer_id)->refresh();
        }
    }

    /**
     * @param bool $postProcess
     * @return static
     * @throws TransactionException
     */
    public function save(bool $postProcess = true)
    {
        $old  = static::instance($this->id);
        $data = $this->getData();
        unset($data->id);

        if (!empty($this->id)) {
            $res = Shop::Container()->getDB()->update($this->table, 'id', $this->id, $data);
        } else {
            $res      = Shop::Container()->getDB()->insert($this->table, $data);
            $this->id = $res >= 0 ? $res : $this->id;
        }

        if ($postProcess) {
            $this->postProcessSave($old, $res >= 0 && !empty($this->id));
        }

        return $this;
    }

    /**
     * @param bool $postProcess
     * @return static
     * @throws TransactionException
     */
    public function delete(bool $postProcess = true)
    {
        $old = static::instance($this->id);
        $res = Shop::Container()->getDB()->delete($this->table, 'id', $this->id);

        if ($postProcess) {
            $this->postProcessDelete($old, (bool)$res);
        }

        return $this;
    }

    /**
     * @param int   $customerID
     * @param float $credit
     * @return float
     * @throws TransactionException
     */
    public function correctCustomerTransaction(int $customerID, float $credit): float
    {
        $db = Shop::Container()->getDB();

        $creditSum = $db->queryPrepared(
            'SELECT -:credit - COALESCE(ROUND(SUM(transaction_value), 4), 0) AS credit
                FROM xplugin_jtl_example_payment_srv_transactions
                WHERE customer_id = :customerID',
            [
                'customerID' => $customerID,
                'credit'     => $credit,
            ],
            ReturnType::SINGLE_OBJECT
        );

        if (\is_object($creditSum) && (float)$creditSum->credit !== 0.0) {
            try {
                self::instance()->assign((object)[
                    'transaction_name'  => 'credit correction',
                    'customer_id'       => $customerID,
                    'transaction_date'  => new DateTime(),
                    'transaction_value' => (float)$creditSum->credit,
                ])->save(false);
            } catch (TransactionException $e) {
                throw new TransactionException('Cant correct transactions', 0, $e);
            }
        }

        $creditSum = $db->queryPrepared(
            'SELECT COALESCE(ROUND(-SUM(transaction_value), 4), 0) AS credit
                FROM xplugin_jtl_example_payment_srv_transactions
                WHERE customer_id = :customerID',
            [
                'customerID' => $customerID,
            ],
            ReturnType::SINGLE_OBJECT
        );

        return \is_object($creditSum) ? (float)$creditSum->credit : 0;
    }

    /**
     * @param int   $merchantID
     * @param float $credit
     * @return float
     * @throws TransactionException
     */
    public function correctMerchantTransaction(int $merchantID, float $credit): float
    {
        $db = Shop::Container()->getDB();

        $creditSum = $db->queryPrepared(
            'SELECT :credit - COALESCE(ROUND(SUM(transaction_value), 4), 0) AS credit
                FROM xplugin_jtl_example_payment_srv_transactions
                WHERE merchant_id = :merchantID',
            [
                'merchantID' => $merchantID,
                'credit'     => $credit,
            ],
            ReturnType::SINGLE_OBJECT
        );

        if (\is_object($creditSum) && (float)$creditSum->credit !== 0.0) {
            try {
                self::instance()->assign((object)[
                    'transaction_name'  => 'credit correction',
                    'merchant_id'       => $merchantID,
                    'transaction_date'  => new DateTime(),
                    'transaction_value' => (float)$creditSum->credit,
                ])->save(false);
            } catch (TransactionException $e) {
                throw new TransactionException('Cant correct transactions', 0, $e);
            }
        }

        $creditSum = $db->queryPrepared(
            'SELECT COALESCE(ROUND(SUM(transaction_value), 4), 0) AS credit
                FROM xplugin_jtl_example_payment_srv_transactions
                WHERE merchant_id = :merchantID',
            [
                'merchantID' => $merchantID,
            ],
            ReturnType::SINGLE_OBJECT
        );

        return \is_object($creditSum) ? (float)$creditSum->credit : 0;
    }
}
