<?php
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 * @package       jtl-shop
 */

namespace Plugin\jtl_example_payment_srv\src;

/**
 * Class TransactionException
 * @package Plugin\jtl_example_payment_srv\src
 */
class TransactionException extends \Exception
{

}
