<?php /** @noinspection ReturnTypeCanBeDeclaredInspection */

/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 * @package       jtl-shop
 */

namespace Plugin\jtl_example_payment_srv\src;

use JTL\Shop;

/**
 * Class TransactionMember
 * @package Plugin\jtl_example_payment_srv\src
 */
abstract class TransactionMember
{
    /** @var int */
    public $id;

    /** @var string */
    public $secret;

    /** @var float  */
    public $credit;

    /** @var string */
    protected $table;

    /**
     * TransactionMember constructor.
     */
    protected function __construct()
    {
        $this->table = '';
    }

    /**
     * @param int|null $id
     * @return static
     */
    public static function instance(?int $id = null)
    {
        $instance = new static();

        if ($id !== null) {
            $instance->load($id);
        }

        return $instance;
    }

    /**
     * @return static
     */
    public function reset()
    {
        foreach (array_keys(get_object_vars($this)) as $var) {
            $this->$var = null;
        }

        return $this;
    }

    /**
     * @param array|object|null $data
     * @return static
     */
    public function assign($data)
    {
        if ($data === null) {
            return $this->reset();
        }

        if (\is_array($data)) {
            $data = ((object)$data);
        }

        foreach (array_keys(get_object_vars($this)) as $var) {
            if (isset($data->$var)) {
                $this->$var = $data->$var;
            }
        }

        $this->id     = (int)$this->id;
        $this->credit = (float)$this->credit;

        return $this;
    }

    /**
     * @return object
     */
    public function getData(): object
    {
        $cs   = Shop::Container()->getCryptoService();
        $data = (object)[
            'id'     => $this->id,
            'credit' => \round($this->credit, 4),
        ];

        if (empty($this->secret)) {
            try {
                $data->secret = $cs->randomString(12);
            } catch (\Exception $e) {
                $data->secret = uniqid('', false);
            }
        } else {
            $data->secret = $this->secret;
        }

        return $data;
    }

    /**
     * @param int $id
     * @return static
     */
    public function load(int $id)
    {
        return $this->assign(
            Shop::Container()->getDB()->selectSingleRow($this->table, 'id', $id)
        );
    }

    /**
     * @param bool   $result
     * @return void
     * @throws TransactionException
     */
    abstract protected function postProcessSave(bool $result): void;

    /**
     * @param bool   $result
     * @return void
     * @throws TransactionException
     */
    abstract protected function postProcessDelete(bool $result): void;

    /**
     * @param bool $postProcess
     * @return static
     * @throws TransactionException
     */
    public function save(bool $postProcess = true)
    {
        $data = $this->getData();
        unset($data->id);

        if (!empty($this->id)) {
            $res = Shop::Container()->getDB()->update($this->table, 'id', $this->id, $data);
        } else {
            $res      = Shop::Container()->getDB()->insert($this->table, $data);
            $this->id = $res >= 0 ? $res : $this->id;
        }

        if ($postProcess) {
            $this->postProcessSave($res >= 0 && !empty($this->id));
        }

        return $this;
    }

    /**
     * @return static
     * @throws TransactionException
     */
    public function delete()
    {
        $this->postProcessDelete((bool)Shop::Container()->getDB()->delete($this->table, 'id', $this->id));

        return $this;
    }

    /**
     * @return static
     * @throws TransactionException
     */
    abstract public function refresh();
}
